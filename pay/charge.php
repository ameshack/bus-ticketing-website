
<?php
  require_once(dirname(__FILE__) . '/config.php');

  $token  = $_POST['stripeToken'];

  $customer = Stripe_Customer::create(array(
      'email' => 'evansonbiwot@gmail.com',
      'card'  => $token
  ));

  $charge = Stripe_Charge::create(array(
      'customer' => $customer->id,
      'amount'   => $_POST['amount'],
      'currency' => 'usd'
  ));

  echo '<h1>Successfully charged $'. ($_POST['amount']/100).'.00 !</h1>';
?>