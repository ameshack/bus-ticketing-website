<?php
$datamod = array( 'current'=>'Blog');
 $this->load->view('new2/extra/upper_view',$datamod);?>

<table width="100%">
<tr>
<td width="22%" valign="top">

<p style="color:#F30; font-size:18px;">Blog Items</p>

<?php

$this->load->view('new2/extra/blog_nav');
?> 


</td><td width="78%" valign="top"><h4>Kenya Safari Styles - Experience a different safari every day </h4>
  <p><img src="<?php echo base_url();?>/style/rebrand/lioness_kenya_mara.jpg" width="842" title="Lioness in Masai Mara Kenya" alt="Lioness in Masai Mara Kenya" /></p>
  <p><br />
    Kenya offers the traveller an unparalleled range of options. The incredible diversity of landscapes, cultures, wildlife and activities mean endless opportunities. With so many choices at hand, some people find the prospect of planning a trip quite daunting. It doesn't have to be that way.</p>
  <p>In Kenya it is very easy to plan and prepare for a safari that is as individual as you are, and that satisfies your own personal needs and interests. If you are planning a trip, use this website to explore Kenya in advance, and decide where you would like to go, what you would like to see and what you would like to do. Here are a few important first points to think about:</p>
  <h4>How Long and How Far?</h4>
  <p>Kenya offers a diverse range of environments all within relatively close proximity to one another, and has an excellent domestic travel infrastructure. This means that you can choice to either stay based in a single location or to move around the country and see a range of different places.</p>
  <p>Your first decision should be how long you want to stay and how much you want to see. Staying in a single place lets you explore that area in great detail. In wilderness areas, this is the best way to really get to experience many facets of an ecosystem.</p>
  <p>Kenya's rich diversity of wildlife means that no two experiences in the wild are ever the same. <br />
    Each day you'll experience and see completely different things. Spending time in a single location also allows you to really get to know and learn from the local cultures, and to get to know locals on an individual level.</p>
  <p>Alternatively, it is possible in Kenya to take a safari to a completely different destination every single day. This is a good option for travellers who like variety of experience and environment. In a single trip to Kenya, you can visit tropical forests, beautiful beaches, and deserts, climb mountains and explore the wild.</p>
  <p>Another choice is to decide on 2 or 3 destinations and spend a few days exploring each one. The choice is yours. When you are planning your trip, think about how many destinations you'd like to visit and how long you'll spend in each one.</p>
  <h4>Who to Bring</h4>
  <p>Whether you are travelling solo or looking for a family trip, Kenya has plenty of options to suit. Think in advance about whether or not you'd like to join an organized safari group, or have your own private transport and guides.</p>
  <p>Kenya has plenty to keep the single traveller busy. Organized safaris and camping trips are often great social experiences and a good way to meet other travellers. Kenya is popular with independent travellers, and is quite easy to meet up with travelling companions on the road.</p>
  <p>For couples, Kenya is a perfect destination for a relaxing break. We have many secluded, private guesthouses, camps and hotels ideal for romantic stays or honeymoons.</p>
  <p>Kenya is also a great family destination. Kids love Kenya, and the sights and experiences of a safari can outdo any theme park. There are hotels that cater especially for families and have special facilities, programs and safari guides for children.</p>
  <h4>Your own Style</h4>
  <p>Look at as many options as you can before you come to Kenya.</p>
  <p>Try and plan for the perfect safari for yourself. If you want five star Luxury accommodation you'll be spoilt for choice. But if you want to really rough it and experience life away from the trappings of civilization it can be equally easily arranged.</p>
  <p>Think about how you are going to get around and how long it will take. Do you want to be use light aircraft to avoid long road trips, or do you enjoy the experience of driving through the countryside?</p>
  <p>It is possible to plan a safari that blends adventure and relaxation, luxury and natural simplicity, social experiences and solitude. Consider trying several different experiences and seeing this great land from several different perspectives.</p>
  <p>In Kenya, you can experience a different safari every day.</p>
  <p>Source: <a href="http://mgaicalkenya.com/" target="_blank">Magical Kenya</a></p></td>

</tr>

</table>









<?php $this->load->view('new2/extra/lower_view',$datamod);?>