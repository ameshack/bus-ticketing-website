<?php
$datamod = array( 'current'=>'Blog');
 $this->load->view('new2/extra/upper_view',$datamod);?>

<table width="100%">
<tr>
<td width="22%" valign="top">

<p style="color:#F30; font-size:18px;">Blog Items</p>

<?php

$this->load->view('new2/extra/blog_nav');
?> 


</td><td width="78%" valign="top"><h3>Basic Facts about <a href="http://goafrica.about.com/od/kenya/tp/Nairobi-Top-Attractions-what-to-see-nairobi-kenya.htm">Nairobi</a></h3>
  <p><img src="<? echo base_url();?>/style/rebrand/Nairobi_City_web.jpg" width="844" height="294" /></p>
  Nairobi is the capital city of <a href="http://goafrica.about.com/od/kenya/Kenya_A_Travel_Guide.htm">Kenya</a> and East Africa's most populous city (3.5 million). Nairobi was founded in 1899 as a railway stop en route to Mombasa. Within a decade it grew to become the capital of British East Africa and became Kenya's capital after independence in 1963. Nairobi is a major business hub and many Aid agencies headquarter here as well. Nairobi has a modern city center, some beautiful suburbs, as well as <a href="http://en.wikipedia.org/wiki/Kibera" zt="-o1/XJ" target="_blank">Africa's largest slum</a>. The city is built on a plateau and it stays pleasantly cool year round. Both English and <a href="http://goafrica.about.com/od/peopleandculture/a/swahili.htm">(ki)Swahili</a>are widely spoken. See <a href="http://goafrica.about.com/od/kenya/tp/Nairobi-Top-Attractions-what-to-see-nairobi-kenya.htm">Top 10 Nairobi Attractions...</a>
  <h3>Claim to Fame</h3>
  Nairobi's crime rate is quite high and the US Government <a href="http://travel.state.gov/travel/cis_pa_tw/tw/tw_5599.html" zt="-o1/XJ" target="_blank">warns travelers</a> to visit with care. On a lighter note, Nairobi is unique in having a very good wildlife park situated just 5 miles from the city center.
  <h3><a href="http://goafrica.about.com/od/kenya/tp/Nairobi-Top-Attractions-what-to-see-nairobi-kenya.htm">Travel to Nairobi</a></h3>
  Nairobi is a major travel hub and most people stay for a night or two <a href="http://goafrica.about.com/od/kenyatopattractions/Kenya_Top_Attractions_Including_Masai_Mara_Mombasa_Malindi_and_More.htm">in transit</a>. Getting around the city is easy by taxi, <em><a href="http://kenya.rcbowen.com/matatu.html" zt="-o1/XJ" target="_blank">matatu</a></em> or <em><a href="http://wanjiku-unlimited.blogspot.com/2008/09/tuk-tuk-invasion.html" zt="-o1/XJ" target="_blank">tuk-tuk</a></em>.
  <p>Nairobi has many <a href="http://goafrica.about.com/od/kenya/tp/Nairobi-Top-Attractions-what-to-see-nairobi-kenya.htm">interesting sights including</a>:</p>
  <ul>
    <li><a href="http://www.kws.org/parks/parks_reserves/NANP.html" zt="-o1/XJ" target="_blank">Nairobi National Park</a> is one of Kenya's most successful black rhino sanctuaries, it also has its own wildebeest migration as well as over 400 species of bird.</li>
    <li>Good museums include the <a href="http://www.museums.or.ke/content/blogcategory/11/17/" zt="-o1/XJ" target="_blank">National Museum</a>, the <a href="http://www.africanmeccasafaris.com/kenya/nairobi/excursions/karenblixen.asp" zt="-o1/XJ" target="_blank">Karen Blixen Museum</a> (of <em>Out of Africa</em>fame) and the <a href="http://www.greywall.demon.co.uk/rail/Kenya/nrm.html" zt="-o1/XJ" target="_blank">Kenya Railway Museum</a>.</li>
    <li><a href="http://www.africanmeccasafaris.com/kenya/nairobi/excursions/carnivore.asp" zt="-o1/XJ" target="_blank">Carnivore Restaurant</a> offers wildly interesting dishes.</li>
    <li><a href="http://www.sheldrickwildlifetrust.org/" zt="-o1/XJ" target="_blank">David Sheldrick Wildlife Trust</a> houses elephant and rhino orphans.</li>
    <li><a href="http://www.victoriasafaris.com/kenyatours/propoor.htm" zt="-o1/XJ" target="_blank">Nairobi Slum Tours</a> -- for those interested in seeing how most of the city's inhabitants live.</li>
  </ul>
  <a href="http://goafrica.about.com/od/kenya/tp/Nairobi-Top-Attractions-what-to-see-nairobi-kenya.htm">See all Top 10 Nairobi Attractions ...</a>
  <p><strong><a href="http://goafrica.about.com/od/kenya/tp/Nairobi-Top-Hotels-nairobi-best-accommodation.htm">Where to Stay in Nairobi</a></strong><br />
  </p>
  <ul>
    <li><a href="http://goafrica.about.com/od/kenya/tp/Nairobi-Top-Hotels-nairobi-best-accommodation.htm">Recommended hotels</a> include the luxurious boutique hotel <a href="http://www.ngonghouse.com/" zt="-o1/XJ" target="_blank">Ngong House</a>, where you can stay in a tree house, or the ultra modern <a href="http://www.tribe-hotel.com/" zt="-o1/XJ" target="_blank">Tribe Hotel</a>. The family owned <a href="http://www.fairviewkenya.com/" zt="-o1/XJ" target="_blank">Fairview Hotel</a> is the best mid-range option, and the tiny <a href="http://www.mitimingi.com/" zt="-o1/XJ" target="_blank">Miit Mingi Guesthouse</a> for budget travelers. If you want to avoid the city center, check out <a href="http://www.nairobitentedcamp.com/" zt="-o1/XJ" target="_blank">Nairobi Tented Camp</a>, just 15 minutes from Wilson airport and situated in the heart of Nairobi National Park. Around the leafy suburbs of Karen, there are some excellent boutique hotel options including: <a href="http://www.houseofwaine.com/" zt="-o1/XJ" target="_blank">House of Waine</a>, and<a href="http://giraffemanor.com/" zt="-o1/XJ" target="_blank">Giraffe Manor</a>. The <a href="http://www.panarihotels.com/" zt="-o1/XJ" target="_blank">Panari Hotel</a> and the <a href="http://www.ekahotel.com/" zt="-o1/XJ" target="_blank">EKA Hotel</a> are both close to the international airport. <a href="http://goafrica.about.com/od/kenya/tp/Nairobi-Top-Hotels-nairobi-best-accommodation.htm">Click here</a> for a list of my recommended hotels in Nairobi.</li>
    <li>Nairobi has many <a href="http://www.eatout.co.ke/" zt="-o1/XJ" target="_blank">restaurants</a> serving a wide variety of cuisines. For people-watching grab a snack at the Thorn Tree Cafe. Fresh seafood can be had at <a href="http://www.tamarind.co.ke/" zt="-o1/XJ" target="_blank">Tamarind</a>, and French cuisine at <a href="http://www.tripadvisor.com/Restaurant_Review-g294207-d1214179-Reviews-Alan_Bobbe_s_Bistro-Nairobi.html" zt="-o1/XJ" target="_blank">Alan Bobbe's Bistro</a>. Of course the <a href="http://www.africanmeccasafaris.com/kenya/nairobi/excursions/carnivore.asp" zt="-o1/XJ" target="_blank">Carnivore Restaurant</a> is perfect for meat lovers.</li>
    <li><a href="http://www.eastafricashuttles.com/excursions.htm" zt="-o1/XJ" target="_blank">Nairobi city tours</a> are a good option for those worried about their personal safety.</li>
    </ul>
  <p>courtesy of <a href="http://goafrica.about.com/od/africatraveltips/ig/Africa-s-Capital-Cities/Nairobi--Kenya-s-capital-city-.htm" zt="-o1/XJ" target="_blank"> Go Africa </a></p>  <h4>&nbsp;</h4></td>

</tr>

</table>









<?php $this->load->view('new2/extra/lower_view',$datamod);?>