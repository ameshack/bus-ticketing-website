<?php
$datamod = array( 'current'=>'Blog');
 $this->load->view('new2/extra/upper_view',$datamod);?>

<table width="100%">
<tr>
<td width="22%" valign="top">

<p style="color:#F30; font-size:18px;">Blog Items</p>

<?php

$this->load->view('new2/extra/blog_nav');
?> 


</td><td width="78%" valign="top"><h3>Uganda - The Pearl of Africa</h3>
  <p><img src="<?php echo base_url();?>/style/rebrand/Giraffe banner.jpg" width="842" height="263" alt="Girraffe" longdesc="http://www.ugandasafaritrail.com/" /></p>
  <h4>Tourism</h4>
  <p>Uganda is a fast growing tourist destination gifted by nature together with its growing stability and hospitable communities. It continues to offer its competitively priced and high quality range of wildlife products offering an awesome experience. The development of tourism and leisure has been enhanced by effective promotion, of both the destination and products offered by government and specific businesses. Establishment of awareness programs, qualified, trained and skilled human resources, improved and stronger tourism administration, investment in the tourism sector and associated products (attractions, entertainment, shopping, etc), enhancing marketing methods and development of basic facilities, social services and proper infrastructure.</p>
  <h4>Wildlife Tourism</h4>
  <p>While the cultural diversity and effortless warmth of Ugandan people are remarked upon by all who visit the country, most itineraries revolve around the protected areas under the direction of Uganda Wildlife Authority (UWA). These are magnificent, encompassing not only conventional Savannah- mesmerizing tracts of African bush teeming with antelope, buffalo, elephant, giraffe, zebra, forest hogs and big cats – but also the snow capped peaks of Africa's tallest range of mountains, tropical rain forests of mind boggling biodiversity, and atmospheric lakes and rivers heaving with hippos, crocodiles and birds.<br />
    As yet untouched by mass tourism, Uganda's parks are ideal retreats for the discerning eco-tourist. Uganda is the world's premier primate viewing destination, home to half of the world's mountain gorillas, large populations of chimpanzees and a dazzling variety of monkeys. For bird lovers, Uganda is practically peerless: it is the only African country that has a record of more than 1000 bird species. Each park offers a memorable experience!</p>
  <h4>How to get to Uganda</h4>
  <p>Uganda has several Airlines flying in and out of the country. Kampala's international airport is Entebbe. Scheduled airlines flying to Entebbe include: Air Burundi Air Rwanda (to/from Kigali), Air Tanzania, British Airways, SN Brussels, East African Airlines, Emirates, Gulf Air, South African Airways, Kenya Airways, Rwandair Express, Egypt Air, Ethiopian Airlines, Eagle Air and United Arab Airlines both Direct flights from Europe and from other African countries.<br />
    Buses and taxis are available from the airport to the city. Wide networks of buses/taxis run throughout the country connecting almost every town. Within Kampala, taxis, special hire taxis and motor bikes provide transport within the city and its suburbs. These are found on major roads in the city centre, as well as the two taxi parks. The journey time is approximately 30 minutes and fare is about $20USD.</p>
  <p>By water: ferry services operate from Tanzania, Mwanza to Port Bell, on Lake Victoria. There are several airstrips within the country; this has made flights to tourism destinations very accessible. Examples of airstrips can be accessed in destination areas to include Kidepo, Queen Elizabeth, Murchison falls and more airstrips are almost in every part of the country national airport or at any overland boarder.</p>
  <h4>Passport and Visas</h4>
  <p>A valid passport is mandatory. Visa requirements sometimes change so check before traveling. A visa to Uganda is issued at Uganda missions abroad and entry points. All countries that require visas for Uganda are also visa prone in Uganda.</p>
  <h4>Health</h4>
  <p>Travelers are advised to contact the Uganda representative in their countries or regions to determine whether vaccination is necessary before entry into Uganda.Yellow fever certificate is required if you have been transiting infected areas. Malaria is prevalent in Uganda and it is advisable to take anti-malarial and mosquito repellents Malaria Prophylaxis is recommended. It is not recommended to drink tap water. Boil it or buy bottled water from the shops. The equatorial sun can be deceptive even on overcast days. Sun glasses and sun creams are recommended to avoid sunburns.</p>
  <h4>Security and Safety</h4>
  <p>Uganda is increasingly developing to one of the top safari holiday destinations in the world. With the improvement of infrastructure and political stability, the tourism industry is soaring to greater heights. The country is very safe both in cities and remote areas. However some common sense precautions should be taken. Do not flaunt your wealth by wearing expensive jewelry or carrying large wads of money openly. Avoid changing money in the streets. Likewise avoid overcrowded streets. Leave your valuables with the hotel for safe keeping. You will come across misleading information on various websites regarding as to how insecure Uganda is for travel. They are however usually outdated and do not reflect the present state of Uganda.</p>
  <h4>Uganda Banks and Money</h4>
  <p>The Banking Sector in Uganda is growing stronger every year with local and foreign owned commercial banks and forex bureaus in Uganda. The Central Bank is Bank of Uganda Working hours for most of the Banks are 8:30 to 4:00pm every Monday to Friday while on Saturday they operate half day. Most forex bureaus are open on weekends and some run for 24 hrs. The Uganda shilling is the legal currency in Uganda. There are no restrictions on money transfer in and out of the country. The Uganda shilling is divided into denominations of 1000, 2000, 5000, 10,000, 20,000 and 50,000 for paper notes, while coins are in the denominations of 50, 100, 200, 500 for coins.</p>
  <h4>Food</h4>
  <p>Uganda offers a range of dishes ranging from continental, Italian, Chinese, Ugandan, Indian and more. The finest restaurants in Uganda are located in Kampala which is the capital city. Most restaurants have specialty in certain cuisine but can also handle other dishes using recipes from Uganda and other countries.</p>
  <p>Source: <a href="http://www.pearlofafricatours.com/" target="_blank">Pearl of Africa Tours</a></p>  <h4>&nbsp;</h4></td>

</tr>

</table>









<?php $this->load->view('new2/extra/lower_view',$datamod);?>