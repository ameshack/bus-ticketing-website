<?php
$datamod = array( 'current'=>'Blog');
 $this->load->view('new2/extra/upper_view',$datamod);?>

<table width="100%">
<tr>
<td width="22%" valign="top">

<p style="color:#F30; font-size:18px;">Blog Items</p>

<?php

$this->load->view('new2/extra/blog_nav');
?> 


</td><td width="78%" valign="top"><h3>Mombasa Facts for Visitors</h3>
  <p><img src="<?php echo base_url();?>/style/rebrand/Mombasa-Skyline.jpg" width="844" height="369" alt="Mombasa" /></p>
  <p>&nbsp;</p>
  <p>The town of Mombasa is East Africa's largest port and Kenya's main tourist hub. Located in the southeastern part of Kenya, it is one of the most significant towns, not only for its imports and exports through its port but also as a major destination for tourists visiting Kenya.</p>
  <h4>The Town</h4>
  <p>The coastal city of Mombasa is actually an island with magnificent stretches of white sandy beaches and coral reefs. The town has four major roads, namely Digo Road, Nkrumah Road, Nyerere Road and Moi Avenue. Along these major roads are where most of the towns shops and businesses are located.</p>
  <h4>Getting Around</h4>
  <p>The Moi International Airport is an architectural symbol of Mombasa's growing investment in tourism. It has been newly-renovated, and a new terminal has been built to facilitate larger aircraft and increased passenger traffic. As a direct result, many of the major airlines that operate out of the airport have increased both the number and frequency of their flights, in particular from major European cities. Smaller airlines operate local and regional flights within Kenya and East and Southern Africa respectively.</p>
  <p>Kenya Airways offers the most frequent flight schedule into and out of Mombasa, to a variety of national and international destinations. Visit Kenya Airways to view flight schedules. Another airline that flies to and from Mombasa is Air Kenya.</p>
  <p>Kenya Railways offers train services throughout the country, primarily between Nairobi and Mombasa. Passengers who chose to travel via rail have the option of travelling either by first or second class. A trip from Nairobi to Mombasa usually takes around 13 hours during which a variation of wildlife can be seen at a relatively close distance. Breakfast, lunch and dinner are also served on the train's dining carriage. Tickets can be purchased at local travel agencies as well as at the main station which is located opposite the roundabout of Haile Salassie avenue and Mwenbe Tyari Road.</p>
  <p>Being a small town, Mombasa does not have an extensive transport grid. The main form of transport is the minivans otherwise commonly known and referred to as &quot;Matatus&quot;. Both forms of transport are highly used by the locals, and a ride in a &quot;Matatu&quot; can be quite a fascinating experience. London-style cabs and other taxis can be found almost anywhere in the town. The North Coast is accessed via the Nyali Bridge, and the South Coast through the Likoni Ferry.</p>
  <h4>Services</h4>
  <p>In the heart of the town is where most hospitals, businesses, banks, shops and markets are situated. Hence almost all services such as health advice, financial services, or any kind of shopping, are all provided for in the City. There are four main hospitals around the City centre, with many smaller clinics located all around the town and its outskirts. They are the Mombasa hospital, Aga Khan hospital, Pandya Memorial hospital and Coast General hospital. Most hotels have resident doctors and nurses.</p>
  <p>Financial institutions are located in abundance throughout the center of the town. Barclays Bank, Standard Chartered, and ABN-AMBRO are a few of such banks that can be found. Many other internationally-based banks also have their branches in Mombasa. Foreign exchange bureaus are also available and offer attractive exchange rates for all currencies. However it is strongly recommended for safety purposes that you carry travelers cheques (American Express or Thomas Cook preferably) as opposed to cash.</p>
  <h4>Security</h4>
  <p>Security is always a concern, and the local government has taken a number of steps to ensure that the general public feels safe in all areas of the City as well as along the beaches. The Central Police Station is located at the heart of the town, and regular patrols are carried out on foot as well as in vehicles all over the City. Kenya Wildlife Service agents maintain a safe and clean environment along the beaches, and ensure that the law is enforced with around the clock surveillance.</p>
  <h4>Port Of Mombasa</h4>
  <p>The Kenya Ports Authority (KPA) manages the port of Mombasa. The port serves as a transit point for Kenya's imports from other countries as well as for its exports of goods and services from the industrial, commercial and agricultural sectors. Most of the ships seen at the port are from Kenya's neighboring countries such as Uganda, Rwanda, Burundi, Eastern Zaire and few others as well. Cruise ships, Navy ships and the famous QEW are frequent visitors to the port and the town. The Port of Mombasa is vast in size. Port Tudor, Kilindini Harbour and Port Reitz, which used to be the old port, are what make up the Port of Mombasa. The Port offers many of the essential services such as cargo handling, berthing of ships, and other such facilities.</p>
  <h4>General Facts</h4>
  <p>Size: 10 to 15 miles (16 to 24 kilometers) wide<br />
    Population: 500,000<br />
    Religion: Christianity, Islam and other minority religious groups<br />
    Language: Swahili and English<br />
    Exports: Tourism, Coffee, Tea, Cement and Cotton<br />
    Climate: Tropical Climate with temperatures varying from low 20's to high 30 degrees celsius.<br />
    Rainfall: 40 inches (1,000 millimeters) a year<br />
    Currency: Kenyan Shilling (Kshs),<br />
    Time Difference: 3 Hours ahead of Greenwich Mean Time.<br />
    Electricity Voltage: 220/240v, step down transformers and batteries are highly recommended. Check if your electrical appliance is compatible with different voltages.<br />
    Distance From Nairobi: 487kms<br />
    Major Airport: Moi International Airport<br />
    Neighboring Towns: Malindi (1 hour away) Watamu (2 hours away)<br />
  </p>
  <p>Source: <a href="http://www.mombasainfo.com/" target="_blank">Mombasa Info</a></p>  <h4>&nbsp;</h4></td>

</tr>

</table>









<?php $this->load->view('new2/extra/lower_view',$datamod);?>