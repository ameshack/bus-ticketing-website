<?php
$datamod = array( 'current'=>'About');
 $this->load->view('new2/extra/upper_view',$datamod);?>

  <p>&nbsp;</p>
  <h3>About Modern Coast Express</h3>
  <p><img src="<?php echo base_url();?>/style/rebrand/about_banner.png" width="1000" height="192" alt="About Modern Coast" /> </p>
  <p>Modern Coast Express Ltd is a luxury coach company incorporated on 2nd October 2007 operating within Kenya and Uganda. Modern Coast Express is one of the most recognised luxury bus companies operating in the region. The company has 10 branches in Kenya and one branch in Kampala, Uganda.<br />
  The main office is on Jomo Kenyatta Avenue in the Total petrol station, next to Aswan restaurant.</p>
  <p>There are two booking offices in Nairobi, Kenya. The main Nairobi branch is located on Accra road opposite Coast Bus. The second booking office is located in the Caltex petrol station on Mombasa road next to Parkside Towers. The other towns mentioned above have one branch in the respective<br />
    towns.</p>
  <p>The company started with a fleet of 6 brand new Scania buses and currently has a fleet of over 20 state of the art coaches. There are daily two services of the air-conditioned buses from Mombasa and Nairobi with the rest being the non air-conditioned services. Modern Coast is the only luxury coach company in Eat Africa to provide both first class and business class service on board. The first class seats are of much bigger dimensions compared to the business class seats.</p>
<h3>What we do</h3>
  <p>Apart from being a luxury P.S.V. coach service, courier service is also provided at the offices of Modern Coast Express Ltd by Modern Coast Couriers Ltd. Modern Coast Express also give the coaches on private hire to any destination within Kenya, Uganda, Tanzania Rwanda and Burundi.</p>
  <h3>Mission</h3>
  <p>To be the most luxurious and friendly coach company serving the East African market.</p>
  <h3>Value</h3>
  <p>The progress and success of the company has been dependent on the hard work and experience of the team at Modern Coast Express in its various locations.</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
<p>&nbsp;</p>
<?php $this->load->view('new2/extra/lower_view',$datamod);?>