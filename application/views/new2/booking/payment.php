<? 

// title      =  title of the page you are creating
// selected   =  can be air,bus event or movie (in small letters)
// separate breadcrumb using -
$hybrid = array(
'current'=>'booking',
'title'=>'Payment for A bus Ticket',
'keywords'=>'m-shop, mshop, mshopkenya  m-shopkenya, mshop web ticketing  ,m-shop web ticketing , kenyan online booking system',
'description'=>'m-shop offers bus ticketing, air ticketing, event ticketing and locating of taxis and movies in a fast, easy and reliable way',
'selected'=>'bus',
'breadcrumb'=>"<a href='".site_url(). "/online/bus_ticket' id='a1' > Bus Ticket <a> -Bus List-Seat List-Payment",
'css'=>NULL



);
$this->load->view('new2/extra/upper_view',$hybrid) ?>
<?php  
			    $rev = $a-$b;
			    //echo $rev;
			    
			    ?>
<?php $ci =& get_instance();
				
				if($this->session->userdata('returntrip') < 1){
					$seat_1 = $this->session->userdata('seat_no');
					$price_1 = $this->function_model->get_price($this->session->userdata('json_d'),
					$this->session->userdata('limit'),$seat_1);
					$price_2 = 0;
					$price = $price_2+$price_1;
					}
				else{
					$seat_1 = $this->session->userdata('seat_1');
					$seat_2 = $this->session->userdata('seat_2');
					$price_1 = $this->function_model->get_price($this->session->userdata('json_d'),
					$this->session->userdata('limit'),$seat_1);
					$price_2 = $this->function_model->get_price($this->session->userdata('json_r'),
					$this->session->userdata('limit_2'),$seat_2);
					$price = $price_2+$price_1;}	 
			

			?>
<style>
#enclose {
	border-radius: 5px;
	padding: 10px;
	margin: 10px;
	border: 1px solid #E2E2E2;
	font-size: 14px;
}
</style>

<table width="100%">
  <tr>
  
  <td width="70%" valign="top">
  
  <script>
 $(function() {
    $( "#accordion" ).accordion();
  });
</script>
  <style>
.acc_head{ 
	padding:5px 5px 5px 30px;
	border-radius:0px;
	 
	}

.acc_cont{ 
	border-radius:0px;
	 
	}
	
.item_head{ 
	background-color:#999;
	padding:9px 9px 9px 9px; 
	color:#FFF; 
	}
#accordion{
	 
	border:#999;
	font-size:14px;}	
	

</style>
  <div>
  
  <?php if(2==6){?>
  <div style="font-size:14px; visibility:hidden;"> You booked seat number <font color = "red"> <? print $seat_1; ?> </font> from <font color = "red"> <? print $this->session->userdata('fromtwn')?></font> to <font color = "red"> <? print $this->session->userdata('totwn')?> </font> on <font color = "red"> <? echo  date('D, d-M-Y',strtotime($this->session->userdata('traveldate_11') )) ?> </font> that costs <font color = "red"><?php echo $price_1;?> </font> KES <br>
    <?php if($this->session->userdata('returntrip') == 2) {?>
    ( Return Ticket  ) You booked seat number <font color = "red"> <? print $seat_2;?> </font> from <font color = "red"> <? print $this->session->userdata('totwn')?></font> to <font color = "red"> <? print $this->session->userdata('fromtwn')?> </font> on <font color = "red"> <? print  date('D, d-M-Y',strtotime($this->session->userdata('traveldate_r') )) ?> </font> that costs <font color = "red"><?php echo $price_2;?> </font> KES <br>
    Total Price ( Both to and From ) = <font color = "red"><?php echo $price; ?></font> KES <br>
    <br>
    <?php }?>
  </div>
  <? } ?>
  <form action="<?php echo site_url();?>/online/validate_bus_ticket" method="post" class="label-top"    >
  
  <div class="item_head"> Personal Information </div>
  <div style=" padding:20px 20px 20px 20px; border:1px solid #999;">
    <div >
      <table width="699"  border="0" style="font-size:14px;">
        <tr>
          <td width="53%">Phone Number: </td>
          <td width="47%"><span><font color = "BEI814"><?php echo form_error('userPhone');?></font></span>
            <input type="text" class="normy_input" name="userPhone"    value="<?php echo set_value('userPhone'); ?>" /></td>
        </tr>
        <tr>
          <td>Loyalty Card : (If card Exists)</td>
          <td><span><font color = "BEI814"></font></span>
            <input type="text" class="normy_input" name="card"    value="<?php echo set_value('paymentTransactionNo'); ?>" /></td>
        </tr>
        <tr>
          <td>Name:</td>
          <td><span><font color = "BEI814"><?php echo form_error('name');?></font></span>
            <input type="text" class="normy_input" name="name"    value="<?php echo set_value('name'); ?>" /></td>
        </tr>
        <tr>
          <td>Currency:</td>
          <td><?php echo form_error('currency');?> KES :
            <input type="radio" value="KES" name="currency" checked="checked" />
            <? echo nbs('4');?> TZ:
            <input type="radio" value="TZ" name="currency" />
            <? echo nbs('4');?> UG:
            <input type="radio" value="UG" name="currency" /></td>
        </tr>
        <tr>
          <td>National Id </td>
          <td><label for="id2"> <span class="red"></span></label>
            <font color = "BEI814"><?php echo form_error('national_id');?></font>
            <input type="text" class="normy_input" name="national_id" id="meduim_input" value="<?php echo set_value('national_id'); ?>"/></td>
        </tr>
        <tr>
          <td>Nationality</td>
          <td><font color = "BEI814"><?php echo form_error('nationality');?></font>
            <select class="normy_select" name="nationality" >
              
              <option selected="selected" value="Kenya">Kenya</option>
              <option value="Uganda">Uganda</option>
              <option value="Tanzania">Tanzania</option>
            </select></td>
        </tr>
      </table>
    </div>
  </div>
  <br />
  <div>
    <div class="item_head">Payment Information</div>
    <div id="accordion" style="border-radius:0px;">
      <h1 class="acc_head" onclick="javascript:document.getElementById('paymethod').value='mpesa'"> M-Pesa </h1>
      <div class="acc_cont">
        <p>
          <label for="id"> Send M-PESA KES. <?php echo $price;?> to Pay Bill Business number 880400.
            </br>
            
            Submit the Confirmation Code below
            </br>
            
            1. Go to M-PESA on your phone
            </br>
            
            2. Select Pay Bill option
            </br>
            
            3. Enter Business no.
            </br>
            
            4. Leave the Account no. blank
            </br>
            
            5. Enter the Amount KES. <?php echo $price;?>
            </br>
            
            6. Enter your M-PESA PIN and Send
            </br>
            
            7. You will receive a confirmation SMS from M-PESA with a Confirmation Code
            </br>
          </label>
        </p>
        <div >
          <label>M-Pesa Confirmation Code: </label>
          <? echo nbs('2') ?> <font color = "BEI814"><?php echo form_error('paymentTransactionNo_mpesa');?></font>
          <input type="text" class="normy_input" name="paymentTransactionNo_mpesa" id="meduim_input"  value="<?php echo set_value('paymentTransactionNo_mpesa'); ?>"/>
        </div>
      </div>
      
      <?php if(4==6){?>
      <h1 class="acc_head" onclick="javascript:document.getElementById('paymethod').value='visa'"> Visa </h1>
      <div class="acc_cont">
        <p> PAY KES. <?php echo $price;?> VIA VISA <br />
          <br />
          1. Click On Pay with Card <br />
          2. Enter your Email Address <br />
          3. Enter your Credit card Number<br />
          4. Enter the date in MM-YY<br />
          5. Enter the Three Digit CVC Number<br />
          <br />
          <?php
require_once('stripe/lib/Stripe.php');

$stripe = array(
  "secret_key"      => $this->config->item('secret_key'),
  "publishable_key" => $this->config->item('publishable_key'),
  
 // "secret_key"      => 'sk_test_XhjzDIqEluJdNho05r3qkIsJ',
 // "publishable_key" => 'pk_test_yPgWqMpbt0j2oniIFOaQVzcN',
);


//var_dump($stripe);

	  Stripe::setApiKey($stripe['secret_key']);
	  ?>
 <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
	  data-key="<?php echo $stripe['publishable_key']; ?>"
	  data-image="<?php echo base_url(); ?>/style/rebrand/components/web_logo.png"
	  data-name=" Modern Coast Online Booking "
	  data-description=" Bus Ticket(s) - KES <?php echo number_format($price);?> "
	  data-amount="<?php echo $price;?>00" ></script> 
        </p>
      </div>
      
      <?php }?>
      <h1 class="acc_head" onclick="javascript:document.getElementById('paymethod').value='zap'"> ZAP - AirTel </h1>
      <div class="acc_cont">
        <p>
          <label for="id"> Send Airtel Money KES. <?php echo $price; $this->session->set_userdata('price',$price);?> nick-name MSHOP. <br>
            Submit the Transaction ID below.<br>
            1. Go to Airtel Money option on your phone.<br>
            2. Select Send Money Option.<br>
            3. Select Enter Nick-Name Option.<br>
            4. Type the name MSHOP.<br>
            5. Enter the Amount KES. <?php echo $price;?>.<br>
            6. Confirm Amount and Nickname.<br>
            7. Enter your PIN and Send.<br>
            8. You will receive a confirmation SMS from Airtel Money with a Transaction ID.<br>
            9. After you receive the confirmation SMS, enter the Transaction ID below and click complete.<br>
          </label>
        </p>
        <div >
          <label>Zap Confirmation Code: </label>
          <? echo nbs('2') ?> <font color = "BEI814"><?php echo form_error('paymentTransactionNo_zap');?></font>
          <input type="text" class="normy_input" name="paymentTransactionNo_zap" id="meduim_input"  value="<?php echo set_value('paymentTransactionNo_zap'); ?>"/>
        </div>
      </div>
    </div>
  </div>
  <td width="4%">
  
  </td>
  
  </td>
  <td width="26%" valign="top" ><div class="item_head" style="margin-top:0px;  "> Ticket Details </div>
    <div style="border:#999; padding:10px; background-color:#E9E9E9; font-size:14px; line-height:200%; "  >
      <p style="color:blue; font-size:19px;"> KES <?php echo $price?></p>
      Travelling by Modern Coast Express <br />
      <br />
      You booked seat number <font color = "blue"> <? print $seat_1; ?> </font> <br />
      from <font color = "blue"> <? print $this->session->userdata('fromtwn')?></font> to <font color = "blue"> <? print $this->session->userdata('totwn')?> </font> <br />
      on <font color = "blue"> <? echo  date('D, d-M-Y',strtotime($this->session->userdata('traveldate_11') )) ?> </font> <br />
      Fare <font color = "blue"><?php echo $price_1;?> </font> KES <br>
      <?php if($this->session->userdata('returntrip') == 2) {?>
      <br />
      ( Return Ticket  ) You booked seat number <font color = "blue"> <? print $seat_2;?> </font> <br />
      from <font color = "blue"> <? print $this->session->userdata('totwn')?></font> to <font color = "blue"> <? print $this->session->userdata('fromtwn')?> </font> <br />
      on <font color = "blue"> <? print  date('D, d-M-Y',strtotime($this->session->userdata('traveldate_r') )) ?> </font> <br />
      Fare <font color = "blue"><?php echo $price_2;?> </font> KES <br>
      <br />
      Total Price ( Both to and From ) = <font color = "blue"><?php echo $price; ?></font> KES <br>
      <br>
      <?php }?>
    </div></td>
  </tr>
  
</table>
<div id="main_content" style=" color:#666666; font-size:14px;">
  <div id="ddtabs1" class="basictab">
    <table width="748px">
      <tr>
        <td>
        <input type="hidden" name="price" value="<?php echo $price?>"  />
        <input type="hidden" name="paymethd" id="paymethod" value="mpesa"  />
          <input type="submit" value=" Continue to Checkout  " class='button_1' /></td>
        <td align="right"><?php if(4==5){?>
          <input type="button" value=" Cancel Booking "  class='button_1' onclick= "window.location.href='<?php echo site_url();?>/online/index'"/>
          <? } ?></td>
      </tr>
    </table>
  </div>
</div>
</form>
</div>
<?
$hybrid2 = array(
'load_side_bar'=>true,   //if true loads the side bar 
'load_lower_information'=>true  //if true doesn't load information bar 
);
 $this->load->view('new2/extra/lower_view',$hybrid) ?>