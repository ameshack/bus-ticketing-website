
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../../../style/rebrand_style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../../../style/form_style_2.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url(); ?>style/rebrand_style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url(); ?>style/form_style_2.css" type="text/css" media="screen" />
<title> Modern Coast | <?php echo $current; ?> </title>

            <!----------------------------------------------------------------------------------->
<link rel="icon" 
      type="image/png" 
      href="<? echo base_url();?>style/rebrand/components/ic_bullet.png" />

<!----------------------------------------------------------------------------------->


<?php if ($current <> 'Gallery'){?>

<link rel="stylesheet" href="<? echo base_url();?>/style/myjs/jquery-ui.css"  />




  
  <script src="<? echo base_url();?>/style/myjs/jquery-1.9.1.js"> </script>
  <script src="<? echo base_url();?>/style/myjs/jquery-ui.js"> </script>
   <script>
     
  $(document).ready(function() {
    $("#traveldate").datepicker({dateFormat:'yy-mm-dd'});
  });

</script>

 <script>
     
  $(document).ready(function() {
    $("#traveldate2").datepicker({dateFormat:'yy-mm-dd'});
  });

</script>
<?php } ?>
</head>

<body class="this_body">

<div class="header">
<div class="inner_header"> 
<div class="logo_space"></div>
<div class="nav_space">

<div class=<?php if($current == 'home'){?> "link_box_selected" <? } else {?> "link_box" <? } ?>> <a class="upper_nav" href="<?php  echo site_url();?>/online/home"> Home </a> </div>
<div  class=<?php if($current == 'booking'){?> "link_box_selected" <? } else {?> "link_box" <? } ?>> <a class="upper_nav" href="<?php echo site_url();?>/online/booking"> Booking </a> </div> 
<div  class=<?php if($current == 'Blog'){?> "link_box_selected" <? } else {?> "link_box" <? } ?>> <a class="upper_nav" href="<?php echo site_url();?>/online/blog/1"> Blog </a> </div>
<div  class=<?php if($current == 'Gallery'){?> "link_box_selected" <? } else {?> "link_box" <? } ?>> <a class="upper_nav" href="<?php echo site_url();?>/online/page/gallery"> Gallery </a> </div> 
<div  class=<?php if($current == 'Network'){?> "link_box_selected" <? } else {?> "link_box" <? } ?>> <a class="upper_nav" href="<?php echo site_url();?>/online/page/network"> Network </a> </div>
<div  class=<?php if($current == 'About'){?> "link_box_selected" <? } else {?> "link_box" <? } ?>> <a class="upper_nav" href="<?php echo site_url();?>/online/page/about"> About </a> </div>
<div  class=<?php if($current == 'contact us'){?> "link_box_selected" <? } else {?> "link_box" <? } ?>> <a class="upper_nav" href="<?php echo site_url();?>/online/contact_us"> Contact Us </a> </div>



</div>

 </div>
</div>

<?php if($current <> 'home'){?>
<div class="content" style="padding:0px; box-shadow: 0 0px 0px #9f9f9f;">
<div class="upper_info">
<div class="inner_upper_info">
<div class="upper_info_head"> <?php echo $current;?> </div>
<div class="upper_button_space"> 
<form><input type="button" value=" Back " class="button_1" onClick="history.go(-1);return false;" /></form>
</div>

</div>
</div>
<div class="inner_content">

<?php } ?>