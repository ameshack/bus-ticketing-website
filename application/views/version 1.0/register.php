<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
        <title>Registration</title>
        <link href="<?php echo base_url();?>style/css/style.css" rel="stylesheet" type="text/css" />
        <meta name="keywords" content="m-shop, mshop, mshopkenya  m-shopkenya, mobile ticketing, kenya online booking system" />
        <meta name="description" content="m-shop offers bus ticketing, air ticketing, event ticketing in a fast, easy and reliable way" />
    </head>
<body>
	
    <div class="content">
    <article>
    <h1 ><font color="BE1814"> M-shop</font></h1>
    	<h4 class="underline">Register</h4>
			<form action="<?php echo site_url();?>/online/registerme" method="post" class="label-top">
			    <div>
			        <font color = "BEI814"><?php if(! is_null($msg)){ ?><div><?php echo $msg;?></div><?php }?></font>
			         <label for="userPhone">Phone Number <span class="red"></span></label>
			         <label><font color = "BEI814"><?php echo form_error('userPhone')."Must be in the format 07XXXXXXXX";?></font></label>
			          <select name="country_code">
  <option value="254">Kenya</option>
  <option value="256">Uganda</option>
  <option value="255">Tanzania</option>

</select>      <input type="text" name="userPhone" id="userPhone" value="<?php echo set_value('userPhone');?>" tabindex="1" />
			    </div>
			    <div>
			         <label for="email">E-mail Adress <span class="red"></span></label>
			         <label><font color = "BEI814"><?php echo form_error('userEmail');?></font></label>
			         <input type="text" name="userEmail" id="userEmail" value="<?php echo set_value('userEmail');?>" tabindex="1" />
			    </div>
			    <div>
				    <input type="submit" value="Register" />
			    </div>
			    
			</form>
			
			
			
    </article>
    
    
    
    <footer>
         
        <p class="copy">&copy; 2012 mshop.com | All rights reserved </p>
    </footer> </div>
</body>
</html>
