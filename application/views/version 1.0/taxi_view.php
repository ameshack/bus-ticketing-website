<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN"
"http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
        <title>taxis</title>
        <link href="<?php echo base_url()?>style/css/style.css" rel="stylesheet" type="text/css" />
        <meta name="keywords" content="m-shop, mshop, mshopkenya  m-shopkenya, mobile ticketing, kenya online booking system" />
        <meta name="description" content="m-shop offers bus ticketing, air ticketing, event ticketing and locating of taxis and movies in a fast, easy and reliable way" />
    
        <script src="http://maps.google.com/maps/api/js?sensor=false" 
          type="text/javascript"></script>

    </head>
<body>
	
    <div class="content">
    <article>
    
		<div id="mapa" style="width:300px; height:300px; border:0px;"></div>
					  <?php  
					  ///$limit = 0;
					 echo anchor('index','Home');
			 ?>
			
    </article>
   </div>
    <script type="text/javascript">
    var locations = [
			<?=$viewdata?>		 
      //['Taxi', -1.287524,36.826551, 4]
    ];

    var map = new google.maps.Map(document.getElementById('mapa'), {
      zoom: 15,
      center: new google.maps.LatLng(-1.287524,36.826551),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>

</body>
</html>
