<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
<title>Dynamic Drive DHTML Scripts- DD Tab Menu Demos</title>

<script type="text/javascript" src="chooser_files/ddtabmenu.js">

/***********************************************
* DD Tab Menu script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<!-- CSS for Tab Menu #1 -->
<link rel="stylesheet" type="text/css" href="chooser_files/ddtabmenu.css">



<script type="text/javascript">
//SYNTAX: ddtabmenu.definemenu("tab_menu_id", integer OR "auto")
ddtabmenu.definemenu("ddtabs1", 0) //initialize Tab Menu #1 with 1st tab selected
ddtabmenu.definemenu("ddtabs2", 1) //initialize Tab Menu #2 with 2nd tab selected
ddtabmenu.definemenu("ddtabs3", 1) //initialize Tab Menu #3 with 2nd tab selected
ddtabmenu.definemenu("ddtabs4", 2) //initialize Tab Menu #4 with 3rd tab selected
ddtabmenu.definemenu("ddtabs5", -1) //initialize Tab Menu #5 with NO tabs selected (-1)

</script>

</head><body>

<h2>1) Example 1</h2>

<div id="ddtabs1" class="basictab">
<ul>
<li><a class=""  rel="sc1">Home</a></li>
<li><a class="current"  rel="sc2">DHTML</a></li>
<li><a class=""  rel="sc3">CSS</a></li>
<li><a class="" >Forums</a></li>
<li><a class="" >Gif Optimizer</a></li>
</ul>
</div>

<div class="tabcontainer">

<div style="display: none;" id="sc1" class="tabcontent">
Return to the <a href="http://www.dynamicdrive.com/">frontpage</a> of Dynamic Drive.
</div>

<div style="display: block;" id="sc2" class="tabcontent">
See the new scripts recently added to Dynamic Drive. <a href="http://www.dynamicdrive.com/new.htm">Click here</a>.
</div>

<div style="display: none;" id="sc3" class="tabcontent">
Original, practical <a href="http://www.dynamicdrive.com/style/">CSS codes and examples</a> such as CSS menus for your site.
</div>

</div>












</body></html>