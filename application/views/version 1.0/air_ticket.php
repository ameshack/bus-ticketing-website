<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
        <title>Air tickets</title>
        <link href="<?php echo base_url()?>style/css/style.css" rel="stylesheet" type="text/css" />
        <meta name="keywords" content="m-shop, mshop, mshopkenya  m-shopkenya, mobile ticketing, kenya online booking system" />
        <meta name="description" content="m-shop offers bus ticketing, air ticketing, event ticketing in a fast, easy and reliable way" />
    
    </head>
<body>
	
    <div class="content">
    <article>
    	
			
			<form action="#" method="post" class="label-top">
			    
			    	
			
				<div>
					<label>Service Provider</label>
					
					<select name="service provider" id="service provider">
						<option value="kenya air">Kenya Air</option>
						<option value="uganda air">Uganda Air</option>
						<option value="Juba air">Juba Air</option>
						
					</select>
				</div>
				<div>
					<label>From</label>
					
					<select name="from" id="from">
						<option value="nairobi">Nairobi</option>
						<option value="mombasa">Mombasa</option>
						<option value="malindi">Malindi</option>
						<option value="kisumu">Kisumu</option>
						<option value="kisii">Abidjav</option>
						<option value="kisii">Accra</option>
						<option value="kisii">Amsterdam</option>
						<option value="kisii">Bamako</option>
						<option value="kisii">Carlifonia</option>
						<option value="kisii">Bangok</option>
						<option value="kisii">Bangui</option>
						<option value="kisii">Barcelona</option>
						<option value="kisii">Berlin</option>
						<option value="kisii">Cairo </option>
						<option value="kisii">Capetown</option>
						<option value="kisii">Kisumu</option>
						<option value="kisii">Liver</option>
						
						
					</select>
				</div>
				<div>
					<label>To</label>
					
					<select name="destination" id="destination" cols = "40" rows ="40">
						<option value="nairobi">Nairobi</option>
						<option value="mombasa">Mombasa</option>
						<option value="malindi">Malindi</option>
						<option value="kisumu">Kisumu</option>
						<option value="kisii">Abidjav</option>
						<option value="kisii">Accra</option>
						<option value="kisii">Amsterdam</option>
						<option value="kisii">Bamako</option>
						<option value="kisii">Carlifonia</option>
						<option value="kisii">Bangok</option>
						<option value="kisii">Bangui</option>
						<option value="kisii">Barcelona</option>
						<option value="kisii">Berlin</option>
						<option value="kisii">Cairo </option>
						<option value="kisii">Capetown</option>
						<option value="kisii">Kisumu</option>
						<option value="kisii">Liver</option>
						
					</select>
				</div>
				 
				
				
				
				<div>
					<input type="checkbox" name="checkbox" id="checkbox" />
					<label for="checkbox" class="inline">Return Trip:</label>
			    </div>
			    
			    <div>
			         <label for="depature_date">Depature Date <span class="red"></span></label>
			         <input type="date" name="depature_date" id="name" value="" tabindex="1" />
			    </div>
			<div>
			         <label for="return_date">Return Date <span class="red"></span></label>
			         <input type="date" name="return_date" id="name" value="" tabindex="1" />
			    </div>
			    
			    
			    <div class="boxed">
					 <table width="100%">
       <tr>
         <td><label>Adults</label></td>
         <td><label> Children</label></td>
         <td><label>Infants</label></td>
       </tr>
       <tr>
         <td><select name="adults" id="adults" >
<?php 
     for ($i = 1; $i < 20; $i++) {
     ?><option value="<?php echo $i;?>"><?php echo $i;?></option>
      <?php }?></select></td>
      
         <td><select name="children" id="children" >
      <?php 
     for ($i = 0; $i < 20; $i++) {
     ?><option value="<?php echo $i;?>"><?php echo $i;?></option>
      <?php }?></select></td>
      
         <td><select name="infants" id="infants" >
      <?php 
     for ($i = 0; $i < 20; $i++) {
     ?><option value="<?php echo $i;?>"><?php echo $i;?></option>
      <?php }?></select></td>
      
       </tr>
     </table>
					
					
				</div>
			    
			    
			    
				<div>
				    <input type="submit" value=Search />
			    </div>
			</form>
    </article>
   </div>
    <footer>
        
        <p class="copy">&copy; 2012 <font color="BE1814">mshop.co.ke</font>  | All rights reserved </p>
    </footer>
</body>
</html>
