
<? 

// title      =  title of the page you are creating
// selected   =  can be air,bus event or movie (in small letters)
// separate breadcrumb using -
$hybrid = array(
'title'=>'Booking on Mobile',
'keywords'=>'m-shop, mshop, mshopkenya  m-shopkenya, mshop web ticketing  ,m-shop web ticketing , kenyan online booking system',
'description'=>'m-shop offers bus ticketing, air ticketing, event ticketing and locating of taxis and movies in a fast, easy and reliable way',
'selected'=>'about_us',
'breadcrumb'=>'<font id="selected" > About Modern Coast Express</font>',
'css'=>NULL



);
$this->load->view('new/extra/upper',$hybrid) ?>

<div id="main_content" style="color:#333; text-align:left;">


<h3>About Modern Coast Express</h3>
<div style="float:left;"><img name="" src="http://moderncoastexpress.com/style/images/mcbc/BUS.OXYGEN.jpg" width="238" height="211" alt="" />
</div>
<p>
Modern Coast Express Ltd is a luxury coach company incorporated on 2nd October 2007 operating within Kenya and Uganda. Modern Coast Express is one of the most recognised luxury bus companies operating in the region. The company has 10 branches in Kenya and one branch in Kampala, Uganda.<br />
  The main office is on Jomo Kenyatta Avenue in the Total petrol station, next to Aswan restaurant.</p>
<p>There are two booking offices in Nairobi, Kenya. The main Nairobi branch is located on Accra road opposite Coast Bus. The second booking office is located in the Caltex petrol station on Mombasa road next to Parkside Towers. The other towns mentioned above have one branch in the respective<br />
  towns.</p>
<p>The company started with a fleet of 6 brand new Scania buses and currently has a fleet of over 20 state of the art coaches. There are daily two services of the air-conditioned buses from Mombasa and Nairobi with the rest being the non air-conditioned services. Modern Coast is the only luxury coach company in Eat Africa to provide both first class and business class service on board. The first class seats are of much bigger dimensions compared to the business class seats.</p>
<h3>What we do</h3>
<p>Apart from being a luxury P.S.V. coach service, courier service is also provided at the offices of Modern Coast Express Ltd by Modern Coast Couriers Ltd. Modern Coast Express also give the coaches on private hire to any destination within Kenya, Uganda, Tanzania Rwanda and Burundi.</p>
<p>&nbsp;</p>

</div>

<?
$hybrid2 = array(
'load_side_bar'=>true,   //if true loads the side bar 
'load_lower_information'=>true  //if true doesn't load information bar 
);
 $this->load->view('new/extra/lower',$hybrid2) ?>