<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Modern Coast Home Page</title>



<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>style/css/fresh_login.css" />
<link rel="stylesheet" type="text/css" href="../../../style/css/fresh_login.css"/>

	
	<link rel="apple-touch-icon" href="<?php echo base_url()?>style/css/indesign/demos/images/apple-touch-icon.png">
    
    
    
    
	<script src="<? echo base_url(); ?>style/css/Source/mootools-core.js" type="text/javascript"></script>
	<script src="<? echo base_url(); ?>style/css/Source/mootools-more.js" type="text/javascript"></script>
	<script src="<? echo base_url(); ?>style/css/Source/Locale.en-US.DatePicker.js" type="text/javascript"></script>
	<script src="<? echo base_url(); ?>style/css/Source/Picker.js" type="text/javascript"></script>
	<script src="<? echo base_url(); ?>style/css/Source/Picker.Attach.js" type="text/javascript"></script>
	<script src="<? echo base_url(); ?>style/css/Source/Picker.Date.js" type="text/javascript"></script>
    <!-------------------------------------------------------------------------------------------------------------------------------------->
    
    <script type="text/javascript" src="<?php echo base_url();?>style/fancy/lib/jquery-1.9.0.min.js"></script>

	<!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="<?php echo base_url();?>style/fancy/lib/jquery.mousewheel-3.0.6.pack.js"></script>

	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="<?php echo base_url();?>style/fancy/source/jquery.fancybox.js?v=2.1.4"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>style/fancy/source/jquery.fancybox.css?v=2.1.4" media="screen" />

	<!-- Add Button helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>style/fancy/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
	<script type="text/javascript" src="<?php echo base_url();?>style/fancy/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>

	<!-- Add Thumbnail helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>style/fancy/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
	<script type="text/javascript" src="<?php echo base_url();?>style/fancy/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

	<!-- Add Media helper (this is optional) -->
	<script type="text/javascript" src="<?php echo base_url();?>style/fancy/source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
    
    <script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();

			/*
			 *  Different effects
			 */

			// Change title type, overlay closing speed
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});

			// Disable opening and closing animations, change title type
			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});

			// Set custom style, close if clicked, change title type and overlay color
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});

			// Remove padding, set opening and closing animations, close if clicked and disable overlay
			$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});

			/*
			 *  Button helper. Disable animations, hide close button, change title type and content
			 */

			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});


			/*
			 *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
			 */

			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,
				arrows    : false,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});

			/*
			 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
			*/
			$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});

			/*
			 *  Open manually
			 */

			$("#fancybox-manual-a").click(function() {
				$.fancybox.open('1_b.jpg');
			});

			$("#fancybox-manual-b").click(function() {
				$.fancybox.open({
					href : 'iframe.html',
					type : 'iframe',
					padding : 5
				});
			});

			$("#fancybox-manual-c").click(function() {
				$.fancybox.open([
					{
						href : '1_b.jpg',
						title : 'My title'
					}, {
						href : '2_b.jpg',
						title : '2nd title'
					}, {
						href : '3_b.jpg'
					}
				], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50
						}
					}
				});
			});


		});
	</script>
    
    <!-------------------------------------------------------------------------------------------------------------------------------------->

	
	<link  href="<? echo base_url(); ?>style/css/Source/datepicker.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<? echo base_url(); ?>style/css/Source/datepicker_dashboard/datepicker_dashboard.css" rel="stylesheet" type="text/css" media="screen"/>
	<link  href="<? echo base_url(); ?>style/css/Source/datepicker_jqui/datepicker_jqui.css" rel="stylesheet" type="text/css" media="screen" />
	<link  href="<? echo base_url(); ?>style/css/Source/datepicker_vista/datepicker_vista.css" rel="stylesheet" type="text/css" media="screen" />

	<style>
		.custom {
			border: 0;
			background: black;
			width: 193px;
		}

		.custom .header {
			background: black;
		}

		.custom .body {
			background: black;
			border: 0;
		}
	</style>

	<script>

	window.addEvent('domready', function(){
		new Picker.Date('dashboard', {
			pickerClass: 'datepicker_dashboard'
		});

		

	});

	</script>



	<!-- jQuery (required) -->
	
	

	<!-- Demo stuff -->
	

	<!-- Anything Slider -->
	
    
	

	<!-- AnythingSlider optional extensions -->
	<!-- <script src="js/jquery.anythingslider.fx.js"></script> -->
	<!-- <script src="js/jquery.anythingslider.video.js"></script> -->

	<!-- Define slider dimensions here -->
	<style>
	#slider { width: 958px; height: 435px; }
	</style>

	<!-- AnythingSlider initialization -->
	<script>
		// DOM Ready
		$(function(){
			$('#slider').anythingSlider();
		});
	</script>
    <style type="text/css">
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
		}
	</style>
    
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
  <script>
  $(document).ready(function() {
    $("#traveldate").datepicker({dateFormat:'dd-mm-yy'});
  });
   $(document).ready(function() {
    $("#traveldate_r").datepicker({dateFormat:'dd-mm-yy'});
  });
  </script>	

<style>
label{
	font-size:15px;}
	
	
</style>

<script type="text/javascript">
 function unhide(divID,divID_2) {
 var item = document.getElementById(divID);
 if (item) {
 item.className=(item.className=='hidden')?'unhidden':'hidden';
 }
  var item = document.getElementById(divID_2);
 if (item) {
 item.className=(item.className=='unhidden')?'hidden':'unhidden';
 }
 
 }
 
 
 </script>

 <style>
.hidden { display: none; }
 .unhidden { display: block; }
 table{
	 font-size:14px;}
 </style>

</head>

<body id="whole">
<div id="header">

<div id="in_header">
<div id="drop_left">
<div id="drop_login">  <?php // $this->load->view('new/extra/drop_login'); ?></div></div>

<div id="logo">   <img src="<? echo base_url();?>style/css/modern/tms_logo.png" >  </div>



<div id="top_menu"> <table> <tr  align="right">   
                              <td width="30" ></td>
                              <td width="100" ></a></td>
                              <td width="106" ></td>
                              <td width="92" ><a href="<? echo site_url(); ?>/online/direct "  id="selected">Home </a></td>
                              <td width="104" ><a href="<? echo site_url();?>/online/details"  id="a1">About Us</a></td>
                              
                              <td width="100" ><a href="<? echo site_url();?>/online/contact_us"  id="a1">Contact Us</a></td>
                              
                               </tr> </table> </div>
</div>

</div>

<div id="slide_show">





		
    
	
	
   <div id='stadium_slide'> 
    
    <div id='first_form'>
    <p style="font-size:28px; color:#06C;">
    Book Your Bus Ticket Now on Modern Coast to Travel
    </p>
	
	<form action="<?php echo site_url();?>/online/bus_view" method="post" class="label-top"   >
			    
			    	
			
				
					<label>  <? $aaa = date("m/d/Y");?></label>
					
				
			
                
				<input type="hidden" name="bustype" name="Kampala Coach"  />
                
                
                
<p >  <div><font color = "BEI814"><?php echo $twn; ?></font></div>
<div><font color = "BEI814"><?php echo $date; ?></font></div>
                   <label>From:  </label>
                    <font color = "BEI814"><? echo form_error('fromtwn');?></font>
				<select  name="fromtwn" class="initial_2"  style="background-color:#FFF">
             
                       
						<option value="">From</option>
                        <option value="Mombasa">Mombasa</option>
                        <option value="Nairobi">Nairobi</option>
						<option value="Malindi">Malindi</option>
						<option value="Kisumu">Kisumu</option>
						<option value="Kisii">Kisii</option>
						<option value="Busia">Busia</option>
						<option value="Migori">Migori</option>
						<option value="Nakuru">Nakuru</option>
						<option value="Eldoret">Eldoret</option>
						<option value="Malaba">Malaba</option>
						<option value="Kampala">Kampala</option>
						
						
					</select>
                    </p>
                    
                     <p > 
                   <label>To: </label>
				
					 <font color = "BEI814"><? echo form_error('totwn');?></font>
					
					<select  name="totwn"  class="initial_2"  style="background-color:#FFF" >
                    
                        
						
                        <option value="">To</option>
                         <option value="Mombasa">Mombasa</option>
                        <option value="Nairobi">Nairobi</option>
						<option value="Malindi">Malindi</option>
						<option value="Kisumu">Kisumu</option>
						<option value="Kisii">Kisii</option>
						<option value="Busia">Busia</option>
						<option value="Migori">Migori</option>
						<option value="Nakuru">Nakuru</option>
						<option value="Eldoret">Eldoret</option>
						<option value="Malaba">Malaba</option>
						<option value="Kampala">Kampala</option>
						
					</select>
				</p>
				 
				
				<p>
			         <label for="travel_date"> Travel Date:<span class="red"  ></span></label>
                     <br />
					 
                                 <font color = "BEI814"><? echo form_error('traveldate');?></font><input type="text"    style="background-color:#FFF; width:375px; height:24px; border:1px solid #999; "   name="traveldate" id="traveldate" value="<? echo date("d-m-Y");?>"  class="initial_2" />
			    </p>
                
				
				<input type="hidden" name="source_check" value="2"/>
			
           <label> Return Trip : </label> <input type="checkbox" value="1" name="returntrip" onclick="javascript:unhide('new_form_space');"   />
            <div id='new_form_space' class="hidden" >
            <p>
			         <label for="travel_date"> Return Date:<span class="red"  ></span></label>
                     <br />
					 
                                 <font color = "BEI814"><? echo form_error('traveldate_r');?></font><input type="text"    style="background-color:#FFF; width:375px; height:34px; border:1px solid #999; "   name="traveldate_r" id="traveldate_r" value=""  class="initial_2" />
			    </p>
            
            </div>
            
            <p >
				   <a href="#" id="how_it_works" >How it Works? </a> <input type="submit" value="" id="home_page_button"  />
			    </p>
			</form>
	
	</div> </div>
    
   
	
	    
       
        

	

		

	

	<!-- END AnythingSlider -->


</div>
<?php if (1==2){?>
<div id="info2">
<div id="inside_info2">

<table width="972" >
<tr>
<td width="188"><img src="<? echo base_url();?>style/css/images/bus_booking.png" alt="Bus Ticket Booking" /> </td>
<td width="60"></td>
<td width="188"><img src="<? echo base_url();?>style/css/images/bus_booking.png" alt="Bus Ticket Booking" /></td>
<td width="62"></td>
<td width="188"><img src="<? echo base_url();?>style/css/images/bus_booking.png" alt="Bus Ticket Booking" /></td>
<td width="57"></td>
<td width="183"><img src="<? echo base_url();?>style/css/images/bus_booking.png" alt="Bus Ticket Booking" /></td>

</tr>
  <tr>
    <td id="information">Book your tickets easily; fast and with convinience. The tickets is delivered to you via SMS which you'll show to board the bus.</td>
<td></td>
<td id="information">Book your tickets easily; fast and with convinience. The tickets is delivered to you via SMS which you'll show to board the bus.</td>
<td></td>
<td id="information">Book your tickets easily; fast and with convinience. The tickets is delivered to you via SMS which you'll show to board the bus.</td>
<td ></td>
<td id="information">Book your tickets easily; fast and with convinience. The tickets is delivered to you via SMS which you'll show to board the bus.</td>
  <tr>
</tr>
</table>

</div></div>

<?php }?>
<div id="social_container">






<div id="social_container_inner">


<div id="beside_engraved">
<strong><font color="#06C" size="+1"> Call Us On </font></strong> : 0202678128 <font color="#06C" size="+2"><strong> | </strong></font> <a id ="mailto" href="mailto:info@modern.co.ke">info@modern.co.ke</a></div>



<div id="engraved_icons">
<table width="240">  <tr>
<td width="35"><a href="http://www.twitter.com/mshopkenya" target="_blank"><img src="<? echo base_url();?>style/css/images/twitta.png" alt="" /></a></td>
<td width="35"><a href="http://www.facebook.com/mshopkenya"><img src="<? echo base_url();?>style/css/images/facebook.png" alt="" /></a></td>
<td width="35"><a href="mailto:info@modern.co.ke" target="_blank"><img src="<? echo base_url();?>style/css/images/mail.png" alt="" /></a></td>

</tr> </table> 

</div>


</div>





</div>
<div id="footer">
<div id="links_container">
  <table width="">  
<tr id="link_headers">
  <td height="23" colspan="2">&nbsp;</td> 
  <td colspan="2"> <font id="lower_link_header"> Connect With Us</font></td> 
  <td width="474" colspan="4">Modern Coast</td>
  </tr>
<tr>
  <td width="1">&nbsp;</td> 
  <td width="176" height="23">&nbsp;</td>
  <td width="34"><img src="<? echo base_url();?>style/css/images/pointa.png" alt="" /></td>
  <td width="151"><a href="mailto: info@mtl.co.ke" title="Facebook" target="_blank" id="a2">Email</a></td>  
  <td colspan="4" rowspan="4" valign="top">
  
 some info
  
  </td> 
  </tr>

<tr>
  <td>&nbsp;</td> 
  <td height="23">&nbsp;</td>
  <td><img src="<? echo base_url();?>style/css/images/pointa.png" alt="" /></td>
  <td><a href="http://www.twitter.com/mshopkenya" title="Twitter" target="_blank" id="a2">Twitter</a></td>   
  </tr>

<tr>
  <td>&nbsp;</td> 
   <td height="23">&nbsp;</td>
   <td><img src="<? echo base_url();?>style/css/images/pointa.png" alt="" /></td>
   <td><a href="http://www.facebook.com/mshopkenya" title="Facebook" target="_blank" id="a2">Facebook</a></td>  
  </tr>
  
<tr><td>&nbsp;</td>
   
  <td height="25" >&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>   
  </tr>
  
</table>
</div>

</div>
<div id="lower">

<div id="lower_info">&copy 2013 <a href="http://www.mtl.co.ke" id="mtl"> MTL Systems Ltd</a>. All Rights Reserved</div>

</div>

</body>
</html>