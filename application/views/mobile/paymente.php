<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
        <title>Payment</title>
        <link href="<?php echo base_url();?>style/css/style.css" rel="stylesheet" type="text/css" />
        <meta name="keywords" content="m-shop, mshop, mshopkenya  m-shopkenya, mobile ticketing, kenya online booking system" />
        <meta name="description" content="m-shop offers bus ticketing, air ticketing, event ticketing in a fast, easy and reliable way" />
        
        <script type="text/javascript" src="<? echo base_url();?>style/chooser_files/ddtabmenu.js">

/***********************************************
* DD Tab Menu script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<!-- CSS for Tab Menu #1 -->
<link rel="stylesheet" type="text/css" href="<? echo base_url();?>style/chooser_files/ddtabmenu.css">



<script type="text/javascript">
//SYNTAX: ddtabmenu.definemenu("tab_menu_id", integer OR "auto")
ddtabmenu.definemenu("ddtabs1", 1) //initialize Tab Menu #1 with 1st tab selected
ddtabmenu.definemenu("ddtabs2", 2) //initialize Tab Menu #2 with 2nd tab selected
ddtabmenu.definemenu("ddtabs3", 3) //initialize Tab Menu #3 with 2nd tab selected


</script>
 
    </head>
<body>
	
          
     <h1>   </h1>
   
    <div class="content">
    <article>
    
			<form action="<?php echo site_url();?>/online/validate_event_payment" method="post" class="label-top">
			    
			    
			    <div>
			    <?php 
			   echo " The total price is ";
			    echo  $a.""." KES ";
			    
			    $rev = $a-$b;
			    //echo $rev;
			    
			    ?>
			    
			    </div>
			<?php $ci =& get_instance(); 
			

			?>    
			    
			    <div>
			         <label for="UserPhone">Phone Number <span class="red"></span></label>
					 <font color = "BEI814"><?php echo form_error('userPhone');?></font>
			         <input type="text" name="userPhone" id="name" value="<?php  print $ci->session->userdata('UserPhone_l');?>" tabindex="1" />
			    </div>
			    <div>
			         <label for="id">National Id <span class="red"></span></label>
					 <font color = "BEI814"><?php echo form_error('national_id');?></font>
			         <input type="text" name="national_id" id="UserId" value="" tabindex="1" />
			    </div>
			    
			    
			    <input type="hidden" name = "mshoprev" value="<?php echo $rev;?>">
			    <input type="hidden" name = "paymentAmount" value="<?php echo  $a;?>">
				
				
				
				
			         
			        
		<div class="basictab">
					<label for="id">Choose Payment method <span class="red"></span></label>
					
					<select name="paymentType" id="paymentType">
						<option value="Mpesa"> <a rel="sc1">Mpesa</a></option>
						<option value="Airtel"> <a rel="sc2">Airtel Money</a></option>
						<option value="YuCash"> <a rel="sc3">YuCash</a></option>
						
						
						
					</select>
				</div>
			    
			    <p>&nbsp;</p>
				<label for="id">Click an icon to show the procedure <span class="red"></span></label>
 
 <div>
		   
		   <div id="ddtabs1" class="basictab">
 
 <table width = "100%">
 <tr>   <td>   <a rel="sc1"><img src="<?php echo base_url();?>style/images/mpesa.jpg"  width ="55" height = "30"></a>           </td>
        <td>  <a rel="sc2"><img src="<?php echo base_url();?>style/images/airtelmoney.jpg"  width ="55" height = "25"></a>  </td>
	   <td>   <a rel="sc3"><img src="<?php echo base_url();?>style/images/yucash.jpg"  width ="55" height = "25"></a>        </td>
 </tr>
 </table>
 
 </div>
		   
		   

<div class="tabcontainer">

<div style="display: none;" id="sc1" class="tabcontent">
<label for="id">
Send M-PESA KES. <?php echo $a;?> to Pay Bill Business number 880400.</br> 
		Submit the Confirmation Code below</br>
		1. Go to M-PESA on your phone</br>
		2. Select Pay Bill option</br>
		3. Enter Business no.</br>
        4. Leave the Account no. blank</br>
        5. Enter the Amount KES. <?php echo $a;?></br>
        6. Enter your M-PESA PIN and Send</br>
        7. You will receive a confirmation SMS from M-PESA with a Confirmation Code</br></label>
	
</div>

<div style="display: block;" id="sc2" class="tabcontent"><label for="id">
Send Airtel Money KES. <?php echo $a;?> nick-name MSHOP. <br>
		Submit the Transaction ID below.<br>
         1. Go to Airtel Money option on your phone.<br>
         2. Select Send Money Option.<br>
         3. Select Enter Nick-Name Option.<br>
         4. Type the name MSHOP.<br> 
         5. Enter the Amount KES. <?php echo $a;?>.<br>
         6. Confirm Amount and Nickname.<br>
         7. Enter your PIN and Send.<br>
         8. You will receive a confirmation SMS from Airtel Money with a Transaction ID.<br>
         9. After you receive the confirmation SMS, enter the Transaction ID below and click complete.<br></label>

</div>

<div style="display: none;" id="sc3" class="tabcontent">
<label for="id">
Send YuCash KES. <?php echo $a;?> to Business Number 180400.<br>
		 Submit the TxnID below<br>
         1. Go to yuCash menu on your phone.<br>
         2. Select Send Money option.<br>
         3. Enter Business no. 180400.<br>
         4. Enter the Amount KES. <?php echo $a;?>.<br>
         5. Leave the Message blank.<br>
         6. Enter your yuCash PIN, and Send.<br>
         7. You will receive a confirmation SMS from yuCash with a TxnID.<br>
         8. After you receive the confirmation SMS, enter the TxnID below and Click on Complete.<br>
</label>

</div>

</div>
		   
		   
	</div>	   
		<p>&nbsp;</p>
		<p>&nbsp;</p>

<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><br>

		
		   
		   
		 
		 <div>
			         <label for="id">Confirmation Code <span class="red"></span></label>
			         <font color = "BEI814"><?php echo form_error('paymentTransactionNo');?></font>
					 <input type="text" name="paymentTransactionNo"  value="" tabindex="1" />
			    </div>
		    
			    
			<table width = "100%">
			<tr>
			<td>  <input  type="submit" value="Complete"  /> </td>
			<td>  <input type="button" value=" Cancel "  onclick= "window.location.href='<?php echo site_url();?>/online/direct'" /></td>
			</tr>
			
			</table>
			
			
			
			
			
		</form>
			
			
			
    </article>
    
    
    
    <footer>
         
        <p class="copy">&copy; 2012 mshop.co.ke | All rights reserved </p>
    </footer> </div>
</body>
</html>
