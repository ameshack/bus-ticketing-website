<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
        <title>Login</title>
        <link href="<?php echo base_url();?>style/css/style.css" rel="stylesheet" type="text/css" />
        <meta name="keywords" content="m-shop, mshop, mshopkenya  m-shopkenya, mobile ticketing, kenya online booking system" />
        <meta name="description" content="m-shop offers bus ticketing, air ticketing, event ticketing in a fast, easy and reliable way" />
	
	
	
	
	</head>
<body>
	<header>
          
          <?php // logo?>
    </header>
    <div class="content">
    <article>
    <h1 ><font color="BE1814"> M-shop</font></h1>
    	<h4 class="underline">Login</h4>
			<form action="<?php echo site_url()?>/online/home2" method="post" class="label-top">
			    
			    <font color = "BEI814"><?php if(! is_null($msg)){ ?><div><?php echo $msg;?></div><?php }?></font>
			                           <?php if(! is_null($suc)){ ?><div><?php echo $suc;?></div><?php }?>
			    
			    <div>
			     <label for="UserPhone">Phone Number <span class="red"></span></label>
			         <label><font color = "BEI814"><?php echo form_error('userPhone')."(Must be in the format 07XXXXXXXX)";?></font></label>
			         <input type="text" name="userPhone" id="UserPhone" value="<?php  if ($userPhone == NULL){echo set_value('userPhone');} else {echo $userPhone;}?>" tabindex="1" />
			    </div>
				<input type="hidden" name="country_code" value="254"/>
			    <div>
			         <label for="pass">PIN Number <span class="red"></span></label>
			         <label><font color = "BEI814"><?php echo form_error('userPass');?></font></label>
			         <input type="password" name="userPass" id="userPass" value="" tabindex="1" />
			    </div>
				
				
				
				
				
				
			    <div>
				<body>
  <div style="width:100%;height:100%;display:none;"
id="pleaseWait">Please wait</div>
  <div style="width:100%;height:100%;" id="myContents"> 
				
				
				    <input type="submit" value="Log In"  onclick="this.disabled=true;   this.value='Sending, please wait...';this.form.submit();"  />
			    </div>
			    
			</form>
			
			
			<p>Not yet registered?</p>
    <p><a href="<?php echo site_url();?>/online/terms" title="Sign Up">Sign Up</a></p>
    </article>
    
    
    
    <footer>
         
        <p class="copy">&copy; 2012 mshop.co.ke | All rights reserved </p>
    </footer>
</body>
</html>
