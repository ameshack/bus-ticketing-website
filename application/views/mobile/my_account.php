<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
        <title>my account</title>
        <link href="<?php echo base_url();?>style/css/style.css" rel="stylesheet" type="text/css" />
         <meta name="keywords" content="m-shop, mshop, mshopkenya  m-shopkenya, mobile ticketing, kenya online booking system" />
        <meta name="description" content="m-shop offers bus ticketing, air ticketing, event ticketing and locating of taxis and movies in a fast, easy and reliable way" />
    
    
    
    
    <style type="text/css">
	/***********************************************************************************************
	
	Copyright (c) 2005 - Alf Magne Kalleland post@dhtmlgoodies.com
	
	Get this and other scripts at www.dhtmlgoodies.com
	
	You can use this script freely as long as this copyright message is kept intact.
	
	***********************************************************************************************/
	body{
		font-family: Trebuchet MS, Lucida Sans Unicode, Arial, sans-serif;
		margin-top:0px;
					
	}
	.bodyText{
		padding-left:10px;
		padding-right:10px;
		border-top:1px solid #000000;	
	}
	.bodyText p{
		margin-top:5px;
	}
	#mainContainer{
		width:760px;
		height:600px;
		border:1px solid #000000;
		
		
		padding-top:85px;			
	}
	#mainMenu{
		
		font-family: Trebuchet MS, Lucida Sans Unicode, Arial, sans-serif;	/* Fonts of main menu items */
		font-size:0.9em;	/* Font size of main menu items */
		border-bottom:1px solid #000000;	/* Bottom border of main menu */
		height:30px;	/* Height of main menu */
		
		position:relative;	/* Don't change this position attribute */
		
	}
	#mainMenu a{
		padding-left:5px;	/* Spaces at the left of main menu items */
		padding-right:5px;	/* Spaces at the right of main menu items */
		font-weight:bold;
		/* Don't change these two options */
		position:absolute;
		bottom:-1px;	/* Change this value to -2px if you're not using a strict doctype */
	}
	#submenu{		
		font-family: Trebuchet MS, Lucida Sans Unicode, Arial, sans-serif;	/* Font  of sub menu items */
			/* Background color of sub menu items */
		float:left;
		width:100%;	/* Don't change this option */
		
	}	
	#submenu div{
		white-space:wrap;	/* Don't change this option */
		float:left;
	}
	/*
	Style attributes of active menu item 
	*/
		
		
		
		cursor:pointer;	/* Cursor like a hand when the user moves the mouse over the menu item */
	}
	
	#mainMenu .activeMenuItem img{
		position:absolute;
		bottom:0px;
		right:0px;
	}
		
	/*
	Style attributes of inactive menu items
	*/
	#mainMenu .inactiveMenuItem{		
		color: #000;	/* Text color */
		cursor:pointer;	/* Cursor like a hand when the user moves the mouse over the menu item */
	}
	
	#submenu a{	
		text-decoration:none;	/* No underline on sub menu items - use text-decoration:underline; if you want the links to be underlined */
		padding-left:10px;	/* Space at the left of each sub menu item */
		padding-right:5px;	/* Space at the right of each sub menu item */
		color: #000;	/* Text color */
		font-size:0.9em; 
	}
	
	#submenu a:hover{
		color: #FF0000;	/* Red color when the user moves the mouse over sub menu items */
	}
	
	</style>
	<script type="text/javascript">
	/************************************************************************************************************
	Copyright (C) October 2005  DTHMLGoodies.com, Alf Magne Kalleland

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

	Dhtmlgoodies.com., hereby disclaims all copyright interest in this script
	written by Alf Magne Kalleland.

	Alf Magne Kalleland, 2010
	Owner of DHTMLgoodies.com

	************************************************************************************************************/
	var menuAlignment = 'left';	// Align menu to the left or right?		
	var topMenuSpacer = 0; // Horizontal space(pixels) between the main menu items	
	var activateSubOnClick = true; // if true-> Show sub menu items on click, if false, show submenu items onmouseover
	var leftAlignSubItems = false; 	// left align sub items t
	
	var activeMenuItem = false;	// Don't change this option. It should initially be false
	var activeTabIndex = 0;	// Index of initial active tab	(0 = first tab) - If the value below is set to true, it will override this one.
	var rememberActiveTabByCookie = true;	// Set it to true if you want to be able to save active tab as cookie
	
	var MSIE = navigator.userAgent.indexOf('MSIE')>=0?true:false;
	var Opera = navigator.userAgent.indexOf('Opera')>=0?true:false;
	var navigatorVersion = navigator.appVersion.replace(/.*?MSIE ([0-9]\.[0-9]).*/g,'$1')/1;
		
	/*
	These cookie functions are downloaded from 
	http://www.mach5.com/support/analyzer/manual/html/General/CookiesJavaScript.htm
	*/	
	function Get_Cookie(name) { 
	   var start = document.cookie.indexOf(name+"="); 
	   var len = start+name.length+1; 
	   if ((!start) && (name != document.cookie.substring(0,name.length))) return null; 
	   if (start == -1) return null; 
	   var end = document.cookie.indexOf(";",len); 
	   if (end == -1) end = document.cookie.length; 
	   return unescape(document.cookie.substring(len,end)); 
	} 
	// This function has been slightly modified
	function Set_Cookie(name,value,expires,path,domain,secure) { 
		expires = expires * 60*60*24*1000;
		var today = new Date();
		var expires_date = new Date( today.getTime() + (expires) );
	    var cookieString = name + "=" +escape(value) + 
	       ( (expires) ? ";expires=" + expires_date.toGMTString() : "") + 
	       ( (path) ? ";path=" + path : "") + 
	       ( (domain) ? ";domain=" + domain : "") + 
	       ( (secure) ? ";secure" : ""); 
	    document.cookie = cookieString; 
	}	
	
	function showHide()
	{
		if(activeMenuItem){
			activeMenuItem.className = 'inactiveMenuItem'; 	
			var theId = activeMenuItem.id.replace(/[^0-9]/g,'');
			document.getElementById('submenu_'+theId).style.display='none';
			var img = activeMenuItem.getElementsByTagName('IMG');
			if(img.length>0)img[0].style.display='none';			
		}

		var img = this.getElementsByTagName('IMG');
		if(img.length>0)img[0].style.display='inline';
				
		activeMenuItem = this;		
		this.className = 'activeMenuItem';
		var theId = this.id.replace(/[^0-9]/g,'');
		document.getElementById('submenu_'+theId).style.display='block';
		

				
		if(rememberActiveTabByCookie){
			Set_Cookie('dhtmlgoodies_tab_menu_tabIndex','index: ' + (theId-1),100);
		}
	}
	
	function initMenu()
	{
		var mainMenuObj = document.getElementById('mainMenu');
		var menuItems = mainMenuObj.getElementsByTagName('A');
		if(document.all){
			mainMenuObj.style.visibility = 'hidden';
			document.getElementById('submenu').style.visibility='hidden';
		}		
		if(rememberActiveTabByCookie){
			var cookieValue = Get_Cookie('dhtmlgoodies_tab_menu_tabIndex') + '';
			cookieValue = cookieValue.replace(/[^0-9]/g,'');
			if(cookieValue.length>0 && cookieValue<menuItems.length){
				activeTabIndex = cookieValue/1;
			}			
		}
		
		var currentLeftPos = 15;
		for(var no=0;no<menuItems.length;no++){			
			if(activateSubOnClick)menuItems[no].onclick = showHide; else menuItems[no].onmouseover = showHide;
			menuItems[no].id = 'mainMenuItem' + (no+1);
			if(menuAlignment=='left')
				menuItems[no].style.left = currentLeftPos + 'px';
			else
				menuItems[no].style.right = currentLeftPos + 'px';
			currentLeftPos = currentLeftPos + menuItems[no].offsetWidth + topMenuSpacer; 
			
			var img = menuItems[no].getElementsByTagName('IMG');
			if(img.length>0){
				img[0].style.display='none';
				if(MSIE && !Opera && navigatorVersion<7){
					img[0].style.bottom = '-1px';
					img[0].style.right = '-1px';
				}
			}
						
			if(no==activeTabIndex){
				menuItems[no].className='activeMenuItem';
				activeMenuItem = menuItems[no];
				var img = activeMenuItem.getElementsByTagName('IMG');
				if(img.length>0)img[0].style.display='inline';	
							
			}else menuItems[no].className='inactiveMenuItem';
			if(!document.all)menuItems[no].style.bottom = '-1px';
			if(MSIE && navigatorVersion < 6)menuItems[no].style.bottom = '-2px';
			

		}		
		
		var mainMenuLinks = mainMenuObj.getElementsByTagName('A');
		
		var subCounter = 1;
		var parentWidth = mainMenuObj.offsetWidth;
		while(document.getElementById('submenu_' + subCounter)){
			var subItem = document.getElementById('submenu_' + subCounter);
			
			if(leftAlignSubItems){
				// No action
			}else{							
				var leftPos = mainMenuLinks[subCounter-1].offsetLeft;
				document.getElementById('submenu_'+subCounter).style.paddingLeft =  leftPos + 'px';
				subItem.style.position ='absolute';
				if(subItem.offsetWidth > parentWidth){
					leftPos = leftPos - Math.max(0,subItem.offsetWidth-parentWidth); 	
				}
				subItem.style.paddingLeft =  leftPos + 'px';
				subItem.style.position ='static';
					
				
			}
			if(subCounter==(activeTabIndex+1)){
				subItem.style.display='block';
			}else{
				subItem.style.display='none';
			}
			
			subCounter++;
		}
		if(document.all){
			mainMenuObj.style.visibility = 'visible';
			document.getElementById('submenu').style.visibility='visible';
		}		
		document.getElementById('submenu').style.display='block';
	}
	window.onload = initMenu;	
	</script>
    
    
    
    
    </head>
<body>
   
    <div class="content">
    <article>
    
			    
			    
 <div id="mainMenu">
	<span style="a color:#FFF;  a background-color:#000; border:1px solid #000"><a   >AirLine</a></span>
	<span style="a color:#FFF; a background-color:#000; border:1px solid #000""><a   >Event</a></span>
	<span style="a color:#FFF; a background-color:#000; border:1px solid #000""><a   >Bus</a></span>
	
</div>
<div id="submenu">
	
	<div id="submenu_1">
		
You have no Airline bookings yet.
       
				
	</div>
	<!-- Second sub menu -->
	<div id="submenu_2">
		
You have no Event bookings yet.
		
	</div>
	<!-- Third sub menu -->
	<div id="submenu_3">
	 
You have no Bus bookings yet.
		
		</div>
		
		   
		 
		 <div>
			
			
			
    </article>
    
    
    
    <footer>
         
        <p class="copy">&copy; 2012 mshop.co.ke | All rights reserved </p>
    </footer> </div>
</body>
</html>
