<?php
class Function_model extends CI_Model {

	function Function_model()
	{
		parent::__construct();
		
		
		
	}

	function getSearchResults ($function_name)
	{
		 $this->db->select('airport, code'); 
        $this->db->from('iata_airport_codes');
        $this->db->like('airport', $function_name);
      
        $query = $this->db->get();
		if ($query->num_rows() > 0) {
			$output = '<ul>';
			foreach ($query->result() as $function_info) {
				{
					$output .= '<li><strong>' . $function_info->airport .'</strong>('.$function_info->code. ')</li>';
				}
			}
			$output .= '</ul>';
			return $output;
		} else {
			return '<p>Sorry, no airport found returned.</p>';
		}
	}
	
	function getSearchResults2($function_name)
	{
		 $this->db->select('airport, code'); 
        $this->db->from('iata_airport_codes');
        $this->db->like('airport', $function_name);
      
        $query = $this->db->get();
		if ($query->num_rows() > 0) {
			//$output = '<ul>';
			foreach ($query->result() as $function_info) {
				{
					$output .= '<strong>' . $function_info->airport .'</strong>('.$function_info->code. ')\n';
				}
			}
			//$output .= '</ul>';
			return $output;
		} else {
			return '<p>Sorry, no airport found returned.</p>';
		}
	}
	
	function select_user_agent($mobile_only)
	{ if ($mobile_only == true){return true;}
	else if($this->agent->is_mobile() == true){return false;}
		}
		
	function get_price($full,$limit,$r){
	
$full_data = $full[$limit];

//echo str_replace(" ","",str_replace("Vip","",$full_data['vipseats'])).'<br>';
//echo str_replace(" ","",str_replace("Fcls","",$full_data['fclassseats'])).'<br>';
//echo $full_data['bizseats'].'<br>';

$vip_seats = explode(',',str_replace(" ","",str_replace("Vip","",$full_data['vipseats'])));
$fc_seats = str_replace(" ","",explode(',',str_replace("Fcls","",$full_data['fclassseats'])));
$bc_seats = explode(',',$full_data['bizseats']);


if      (in_array($r, $vip_seats)) { $price =  $full_data['vipprice']; }
else if (in_array($r, $fc_seats)) { $price =  $full_data['fclassprice']; }
else if (in_array($r, $bc_seats)) { $price =  $full_data['bizprice']; }
else {$price = 'nothing';}

return $price;
	}	

}
?>