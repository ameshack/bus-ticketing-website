<?php
 class Online extends CI_Controller {
	 
	 
 	function  online(){
		$dpage = 'new2';
 	parent::__construct();
    $this->load->helper('url');
 	$this->load->helper('form');
 	$this->load->library('session');
	$this->load->library('user_agent');
	$this->load->helper('html');
	$this->load->model('function_model');
	
	

}


function blog($page = 1){
	 
	$this->load->view('new2/blog/'.$page );
	
}

function royalty_signup(){
	$this->load->view('new2/royalty_login_view' );
	}


function load_view($location, $data = NULL ){
	$mode = $this->config->item('page_mode');
	//$mode = 'mobile';
	if($mode == 'desktop') { $this->load->view('new2/'.$location,$data);}
	elseif($mode == 'mobile') { $this->load->view('mobi2/'.$location,$data);}
	else if($mode == 'auto') {
	if ($this->agent->is_mobile())
          {   $this->load->view('mobi2/'.$location,$data);   }
    else  {   $this->load->view('new2/'.$location,$data);    } 
 	}
	}

function template($name){
	$this->load->view('new2/'.$name.'_view');
	}

function page($name){
	$this->load->view('new2/'.$name.'_view');
	}	

//  mobile pages //

function mobi_template($mobi_name = 'template_view'){
	$this->load->view('mobi2/'.$mobi_name);
	}	
function mobile_home(){ }

function faq(){ $this->load_view('faqs_view'); }
function terms_and_conditions(){ $this->load_view('terms_and_conditions_view'); }

function how_it_works(){ $this->load_view('how_it_works_view'); }

function mobile_contact(){ $this->load->view(); }	
	
// end mobi pages // 

function contact_us(){
	$data = array(
	
	'message'=>NULL
	);
	
	$this->load_view('contact_view',$data);
	
	}
	
function send_mail(){
		
		
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('your_name', 'Name', 'required');	
		$this->form_validation->set_rules('email', 'E-mail', 'required|valid_email');	
		$this->form_validation->set_rules('your_message', 'Message', 'required');
	
	
	if ($this->form_validation->run() == FALSE){
		
		$data = array(
		
		'message'=>'<font color="#990000"> Please check your entries and try again</font>'
		
		);
		
		$this->load_view('contact_view',$data);
		
		
		}else{
			
			
$return = $this->main_model->send_mail ($this->input->post('your_message'),$this->input->post('your_name'),$this->input->post('email'));
			
			if($return == TRUE){  $data['message']='<font color="#009933"> Message sent sucessfully </font><BR>';  }
			else{ $data['message']='<font color="#009933"> Message couldn\'t be sent </font><BR>';  }
			
			
		
		$this->load_view('contact_view',$data);
		
		}
}
	
	function show_map($town_name){
		$data['town'] = $this->main_model->getspecificone('town','town_name',$town_name);
		$this->load->view('new2/map_view',$data);
		
		
		}
	
	
	
	function home(){
		
 	//setting all messagaes to be displayed as null
	
	
  	$data = array(		
 	'msg' => NULL,
  	'userPhone'=>NULL,
  	'suc'=>NULL,
	'twn'=>NULL,
	'date'=>NULL
  		  	  	 
	    );
	 
	 
  if ($this->agent->is_mobile())
          {   $this->load->view('mobi2/home_view' ,$data);   }
    else  {   $this->load->view('new2/home_view',$data);    } 
 	 
 		}

	
 	
 function index() {
 	//setting all messagaes to be displayed as null
	
	
  	$data = array(		
 	'msg' => NULL,
  	'userPhone'=>NULL,
  	'suc'=>NULL,
	'twn'=>NULL,
	'date'=>NULL
  		  	  	 
	    );
	 
	 
  
	 
	if ($this->agent->is_mobile())
          {   $this->load->view('mobi2/booking/login_fresh' ,$data);   }
    else  {   $this->load->view('new2/home_view',$data);    } 
 	 
 		}	
		
	
	
	 function booking() {
 	//setting all messagaes to be displayed as null
	
	
  	$data = array(		
 	'msg' => NULL,
  	'userPhone'=>NULL,
  	'suc'=>NULL,
	'twn'=>NULL,
	'date'=>NULL
  		  	  	 
	    );
	$this->load_view('booking/login_fresh',$data);  
 
 		}	

 		function direct() {
		
	
		redirect('online/index');
		
		
 			//$this->load->view('home2');
 		}
 		
 
 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 function bus_ticket(){
	 redirect('online/index');
 $this->restrict();
 $data= array(
	
	'twn'=>null,
	'date'=>null
	);
	
	if ($this->agent->is_mobile())
     {   $this->load->view('mobile/bus_ticket',$data);   }
    else   
	{  $this->load->view('new/bus_ticket',$data); } 
	
 	
 		}
                                                                                                                                             //bus_view 
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
 
function bus_view() {
	$this->session->sess_destroy();
	$this->session->set_userdata('returntrip','');
//retrieve date from the view

if(($this->input->post('source_check'))=='1'){
//from mobile

$cal_date = $this->input->post('year')."-".$this->input->post('month')."-".$this->input->post('date');
//echo $cal_date."<br>";

$calc_date=date('Y-m-d',strtotime($cal_date));

//echo $calc_date;
}

else if (($this->input->post('source_check'))=='2'){
//from web version
$calc_date = date('20y-m-d',strtotime($this->input->post('traveldate')));
}

$today = date("Y-m-d");




//storing the first session data
	
	$this->load->library('form_validation');
	$this->form_validation->set_rules('traveldate', 'Travel Date', 'required');
	$this->form_validation->set_rules('fromtwn', 'From Town', 'required');
	$this->form_validation->set_rules('totwn', 'Destination', 'required');
	if ($this->form_validation->run() == FALSE){
		
		$data = array(		
 	'msg' => NULL,
  	'userPhone'=>NULL,
  	'suc'=>NULL,
	'twn'=>NULL,
	'date'=>NULL
  		  	  	 
	    );
	
     $this->load_view('booking/login_fresh',$data); }
		
			
		
		
		else if 
		//(4 > 5){ 
		($today > $calc_date){
	
	$data= array(
	'twn'=>"",
	'date'=>"You can either select today or any day after today",
	'msg' => NULL,
  	'userPhone'=>NULL,
  	'suc'=>NULL
	
	
 );
	
	
	
	 $this->load_view('booking/login_fresh',$data); 
	
	}
		
		else if (($this->input->post('fromtwn'))==($this->input->post('totwn'))){	
	$data= array(
	
	'twn'=>"Please select different towns",
	'date'=>"",
	'msg' => NULL,
  	'userPhone'=>NULL,
  	'suc'=>NULL,
	
	
 );
	
	//$this->load->view('bus_ticket',$data);
	
	$this->load_view('booking/login_fresh',$data);
	
	
	
	
	
	}
	
	else{
	

	$this->load->library('session');
	$calc_date = date('d-m-Y',strtotime($calc_date));
	
	$data = array(
 	        'fromtwn'=>$this->input->post('fromtwn'),
            'totwn'=>$this->input->post('totwn'),
            'traveldate'=>$calc_date,
 	       
 	       
                    );
            $this->session->set_userdata($data);

$this->session->set_userdata('traveldate_11','');
$this->session->set_userdata('traveldate_11',$calc_date);

$fromtwn = $this->input->post('fromtwn');
$totwn = $this->input->post('totwn');
$traveldate = $calc_date;

$returntrip = $this->input->post('returntrip');
if($returntrip == 1){  
$traveldate_r = $this->input->post('traveldate_r');
$this->session->set_userdata('returntrip',$returntrip);
$this->session->set_userdata('traveldate_r',$traveldate_r);
} else {$traveldate_r = NULL;}
//echo $fromtwn.''.$totwn.''.$traveldate;

if($this->config->item('dev_mode') == 'live'){

$json_d = json_decode(file_get_contents('http://178.238.232.121/tms/admins/schedulesjson/'.$fromtwn.':'.$totwn.':'.$traveldate.''), TRUE);
}
else{
$json_d =json_decode('[{"busid":"90","busname":"Normal","routecode":"MO_ML4:00PM(A)","reptime":"10:00 AM","deptime":"9:30 AM","vipseats":"1 Vip,4 Vip","fclassseats":"2 Fcls,3 Fcls,5 Fcls,6 Fcls","bizseats":"10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,7,8,9","vipprice":"1600.00","fclassprice":"1500.00","bizprice":"1300.00"}
]',TRUE);
}

$this->session->set_userdata('json_d',$json_d);
$original_data ['json_d'] =  $json_d ;
$this->load_view('booking/bus_view',$original_data); 
	
	}}
 
 //function only used in case a return trip

 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
 
 function seat_view($limit) {
	 if($this->session->userdata('returntrip') > 1){
		 $this->session->set_userdata('limit_2',$limit); 
		  $full_data = $this->session->userdata('json_r');
	 $data = array(
	 'val' => 2,
	 'available_seats'=>'12,4,5,6,7',
	 'bus_type'=>'Oxygen',
	 'depature_time'=>'21:00',
	 'full_data' =>$full_data[$limit]
	 );
 
	
	
	$this->load_view('booking/seat_list',$data);
	
	
		 }
		 else{
	 $this->session->set_userdata('limit',$limit);
	 $full_data = $this->session->userdata('json_d');
	 $data = array(
	 'val' => 2,
	 'available_seats'=>'12,4,5,6,7',
	 'bus_type'=>'Oxygen',
	 'depature_time'=>'21:00',
	 'full_data' =>$full_data[$limit]
	 );
 
	
  $this->load_view('booking/seat_list',$data); 
	
	
	}}
 
 
 
		
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 	
		


 
function paypal() {
$this->load->library('paypal_class');
$this->paypal_class->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
//$this->paypal_class->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url
$this->paypal_class->add_field('currency_code', 'USD');
$this->paypal_class->add_field('business', 'evanso_1341214372_biz@gmail.com');
//$this->paypal_class->add_field('business', $this->config->item('bussinessPayPalAccount'));
$this->paypal_class->add_field('return', 'http://localhost/mshop/index.php/online/success'); // return url
$this->paypal_class->add_field('cancel_return', 'http://localhost/mshop/index.php/online/cancel'); // cancel url
$this->paypal_class->add_field('notify_url', 'http://localhost/mshop/index.php/online/notify'); // notify url
//$totalPrice = $this->session->userdata('totalPrice');
//$totalprice = $this->input->post('amount');
//$item_name = $this->input->post('item_name');
$totalPrice = 10;
$item_name = 'Bus Seat';
$this->paypal_class->add_field('item_name', $item_name);
$this->paypal_class->add_field('amount', $totalPrice);
$this->paypal_class->add_field('custom', $this->session->userdata('orderId'));
$this->paypal_class->submit_paypal_post(); // submit the fields to paypal
//$p->dump_fields();      // for debugging, output a table of all the fields
exit;
}       



function validatePaypal() {
$this->load->library('paypal_class');
$this->paypal_class->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
//$this->paypal_class->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url
if ($this->paypal_class->validate_ipn()) {
$orderId = trim($_POST['custom']);
$itemName = trim($_POST['item_name']);
// put your code here
}
break;
}

function cancel(){
    $data = Array(

'val' => '4',
'msg' => "The transaction doesn't exist",
'wrn' => "<p><font color='red'>Oops! invalid or Canceled Paypal Transaction </font> </p> <p> Please verify the transaction  and try again or call our customer care on 0202678128</p>",
);  
if ($this->agent->is_mobile())
     {   $this->load->view('mobile/final',$data);   }
    else   
	{  $this->load->view('web/final',$data); }    
}  
    

          
function success(){
    $data = Array(

'val'=>1,
'msg' => 'Transaction via Paypal successfull !!',
'wrn' =>'',
'name'=>'Marcel Auja',
'amount'=>'1330 KES',
'date'=>'2012-07-12',
'arrival'=>'10:00 am',
'depature'=>'10:30 am',
'type'=>'Oxygen',
'seat'=>'33'
);  
if ($this->agent->is_mobile())
     {   $this->load->view('mobile/final',$data);   }
    else   
	{  $this->load->view('web/final',$data); }    
}
  
function notify(){
 $this->load->library('paypal_class');
$this->paypal_class->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
//$this->paypal_class->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url
if ($this->paypal_class->validate_ipn()) {
$orderId = trim($_POST['custom']);
$itemName = trim($_POST['item_name']);
// put your code here


var_dump($_POST);

//echo $orderId;
    
}}
                
                
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function get_aval_buses($fromtwn,$totwn,$traveldate,$return = TRUE){
if($this->config->item('dev_mode') == 'live'){

	$json_r = json_decode(file_get_contents('http://178.238.232.121/tms/admins/schedulesjson/'.$fromtwn.':'.$totwn.':'.$traveldate.''), TRUE); }
	else {

$json_r =json_decode('[{"busid":"90","busname":"Minibus","routecode":"MO_ML4:00PM(A)","reptime":"9:30 AM","deptime":"10:00 AM","vipseats":"1 Vip,4 Vip","fclassseats":"2 Fcls,3 Fcls,5 Fcls,6 Fcls","bizseats":"10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,7,8,9","vipprice":"1600.00","fclassprice":"1500.00","bizprice":"1300.00"}]',TRUE); }





$this->session->set_userdata('json_r',$json_r);
$original_data['return'] = $return;
$original_data['fromtwn'] = $fromtwn;
$original_data['totwn'] = $totwn;
$original_data['traveldate'] = $traveldate;
$original_data ['json_d'] =  $json_r ;
$this->session->set_userdata('returntrip',2) ; // user has already select return trip;
$this->load_view('booking/bus_view',$original_data); 
	
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                
		
 function payment($r){
	 if($this->session->userdata('returntrip') == 1){
		 
		 $this->session->set_userdata('seat_1',$r);
		 $fromtwn = $this->session->userdata('totwn');
		 $totwn = $this->session->userdata('fromtwn');
		 $traveldate  = $this->session->userdata('traveldate_r');
		 $return = TRUE;
		 
		 $this->get_aval_buses($fromtwn,$totwn,$traveldate,$return);
		 
		 }
	else {
	 
$full_data = $this->session->userdata('json_d');
$seat = $r;
$this->session->set_userdata('seat_2',$r);
$this->session->set_userdata('seat_no',$seat);
$limit = $this->session->userdata('limit');
$full = $this->session->userdata('json_d');
$full_data = $full[$limit];

//echo str_replace(" ","",str_replace("Vip","",$full_data['vipseats'])).'<br>';
//echo str_replace(" ","",str_replace("Fcls","",$full_data['fclassseats'])).'<br>';
//echo $full_data['bizseats'].'<br>';

$vip_seats = explode(',',str_replace(" ","",str_replace("Vip","",$full_data['vipseats'])));
$fc_seats = str_replace(" ","",explode(',',str_replace("Fcls","",$full_data['fclassseats'])));
$bc_seats = explode(',',$full_data['bizseats']);


if      (in_array($r, $vip_seats)) { $price =  $full_data['vipprice']; }
else if (in_array($r, $fc_seats)) { $price =  $full_data['fclassprice']; }
else if (in_array($r, $bc_seats)) { $price =  $full_data['bizprice']; }
else {$price = 'nothing';}
	 
	 
	 $data = array(
	 'a'=>2040,
	 'b'=>40,
	 'price'=>$price
	 );
	  $this->load_view('booking/payment',$data); 
	
	}}
	
	
  		
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 	
	function finale(){
	$this->restrict();
	$this->load->view('new/final');
	
	}
	
	

		
  	function validate_bus_ticket() {
	
	
	//$this->restrict();
	
	$paymentmethd =  $this->input->post('paymethd');
	
	
	
	//echo $paymentmethd;
	//exit;
	
	$this->load->library('form_validation');
	$this->form_validation->set_rules('userPhone', 'Phone Number', 'required|numeric|exact_length[10]');
	$this->form_validation->set_rules('national_id', 'National Id', 'required|numeric|exact_length[8]');
	
	$this->form_validation->set_rules('name', 'Name', 'required');
	$this->form_validation->set_rules('currency', 'Currency', 'required');
	$this->form_validation->set_rules('nationality', 'Nationality', 'required');
	
if($paymentmethd == 'mpesa'){ $this->form_validation->set_rules('paymentTransactionNo_mpesa', 'Mpesa Confirmation Code', 'required'); }
else if($paymentmethd == 'zap'){ $this->form_validation->set_rules('paymentTransactionNo_zap', 'Zap Confirmation Code', 'required'); }

	
	if ($this->form_validation->run() == FALSE){
		
		//if the data entered during registration has the wrong format
		$this->payment($this->session->userdata('seat_no'));
			 
		}	
		else {
			
	 $limit = $this->session->userdata('limit');
	 $full = $this->session->userdata('json_d');
	 $full_data = $full[$limit];	
			
$schedule_id = $full_data['busid']; // v 1
$stattionfrom = $this->session->userdata('fromtwn'); // v 2
$stationto = $this->session->userdata('totwn'); // v 3
$clientname = $this->input->post('name'); // c
$idnumber = $this->input->post('national_id'); $this->session->set_userdata('national_id',$idnumber); // c
$mobileno = $this->input->post('userPhone'); // c
$seatno = $this->session->userdata('seat_no'); // v 4
$currency =  $this->input->post('currency');// c
$voucerno = 'None'; // c

if($paymentmethd == 'mpesa'){ $mpesaref = $this->input->post('paymentTransactionNo_mpesa'); }
else if($paymentmethd == 'zap'){  $mpesaref = $this->input->post('paymentTransactionNo_zap'); }
else if($paymentmethd == 'visa'){  $mpesaref = $this->input->post('paymentTransactionNo_visa'); }  //c



$bookingmode = 4; //4    // c
$userid = 0; //0  // c
$nationality = $this->input->post('nationality'); // c
$channel = 3; //3  // c
$cardno = $this->input->post('card');  // c

//echo 'schedule_id '.$schedule_id.' <br>'; 
//echo 'stattionfrom '.$stattionfrom .' <br>'; 
//echo 'stationto '.$stationto.' <br>';  
//echo 'clientname '.$clientname .' <br>'; 
//echo 'idnumber '.$idnumber.' <br>'; 
//echo 'mobileno '.$mobileno.' <br>'; 
//echo 'seatno '.$seatno .' <br>'; 
//echo 'currency '.$currency.' <br>'; 
//echo 'voucerno '.$voucerno .' <br>'; 
//echo 'mpesaref '.$mpesaref .' <br>'; 
//echo 'bookingmode '.$bookingmode.' <br>'; 
//echo 'userid '.$userid.' <br>'; 
//echo 'nationality '.$nationality .' <br>'; 
//echo 'channel '.$channel.' <br>'; 
//echo 'cardno '.$cardno.' <br><br>'; 




//////////////if transaction is visa do this 


	if ($paymentmethd == 'visa'){
		
require_once('stripe/lib/Stripe.php');

$stripe = array(
 "secret_key"      => $this->config->item('secret_key'),
  "publishable_key" => $this->config->item('publishable_key'),
  
  //"secret_key"      => 'sk_test_XhjzDIqEluJdNho05r3qkIsJ',
 // "publishable_key" => 'pk_test_yPgWqMpbt0j2oniIFOaQVzcN',
);




Stripe::setApiKey($stripe['secret_key']);
		
		
		
		 $token  = $_POST['stripeToken'];

  $customer = Stripe_Customer::create(array(
      'email' => 'evansonbiwot@gmail.com',
      'card'  => $token
  ));

  $charge = Stripe_Charge::create(array(
      'customer' => $customer->id,
      'amount'   => $this->session->userdata('price').'00',
      'currency' => 'usd'
  ));


//var_dump ($_POST);
  //echo '<h1>Successfully charged '.$this->session->userdata('price').'.00!</h1>'; 
	 
	 
$json_r = json_decode('[{"ticketno":"MO279","Busno":"A","Status":"1"}]', TRUE);
$json_rd = $json_r[0];

	 
	
	


$data['json_rd'] = $json_rd;
$data['full_data'] = $full_data;
$data['msddf'] = ' Successfully charged '.$this->input->post('stripeEmail').' '.$this->session->userdata('price').'.00! ';


  $this->load_view('booking/final',$data); 
		
		//echo 'end';
		return 0;
		}
		
	

else if ($paymentmethd == 'mpesa'){ 

// echo 'mpesa';
//confirm mpesa ref number

//check payment
			 
			 $amount_paid = '1';
			 $required_amount = $this->input->post('price');
			 $ref_no = $this->input->post('paymentTransactionNo_mpesa');
			
			 $data = $this->main_model->validate_payment($ref_no,$amount_paid,$required_amount);
			 
			// echo 'data to cont';
			 if(($data[0] == 'fail') OR ($data[0] == 'proccessed') OR ($data[0] == 'reversed')){
				 
				// echo 'fail';
				  $this->session->set_userdata('msgg',' <br/> The payment wasn\'t successfull, please confirm your details and try again or call us on 
				 <font style=" font-weight:bold"> 
				  0202678128 </font> </br>  </br> 
				  
				  Or M-pesa Might be experiencing Delays. Please try again after 5 minutes. Thankyou.
				  </br></br>


</br>');

// echo 'ddd';
$datad['tiko'] = '0';
$this->load_view('booking/final' ,$datad); 
                        
						return 0;
				 }
				   
			else if($data[0] == 'success'){ 
			
			// echo ' ';
			if($data[3] == 'none'){
				//echo 'none';
		$this->session->set_userdata('msgg','Oops!<br/> The payment was successfull, 
		  but you paid less than the required amount. Please call us on 
				 <font style=" font-weight:bold"> 
				  0202678128 </font> </br></br></br>


</br>');
                      $datad['tiko'] = '0';
$this->load_view('booking/final' ,$datad);  
						return;
					}
					
			
			  else if($data[3] == 'okay'){
				  //echo 'okay';
				$validated_ref_no = $data[1];
				$validated_amount = $data[2];
				
				
				$url = str_replace(' ', '20%', 'http://178.238.232.121/tms/admins/create_ticket_device?scheduleid='.$schedule_id.'&stationfrom='.$stattionfrom.'&stationto='.$stationto.'&clientname='.$clientname.'&idnumber='.$idnumber.'&mobileno='.$mobileno.'&seatno='.$seatno.'&currency='.$currency.'&voucherno='.$voucerno.'&mpesaref='.$mpesaref.'&bookingmode='.$bookingmode.'&userid='.$userid.'&nationality='.$nationality.'&channel='.$channel.'&cardno='.$cardno.'');	

if($this->config->item('dev_mode') == 'live'){	
$json_r = json_decode(file_get_contents($url), TRUE); 
}
else{
$json_r = json_decode('[{"ticketno":"MO279","Busno":"A","Status":"1"}]', TRUE); }

 
$json_rd = $json_r[0];

	//echo 'Ticket No -> '.$json_rd['ticketno'].'<br>';
	//echo 'Bus -> '.$json_rd['Busno'].'<br>';
	//echo 'Status -> '.$json_rd['Status'].'<br>';
	
	


$data['json_rd'] = $json_rd;
$data['full_data'] = $full_data;
$data['tiko'] = '1';

//$this->send_sms($mobileno,'You have Successfully booked ticket No '.$json_rd['ticketno'].' from'..'to'.' ');



  $this->load_view('booking/final',$data); 
				
				
				}
	}
			//end check payment  
			 




 }



	
 	
	
	
}}
	
		function clean_phone($phone){
		 $rtrimmedphoneno = str_replace (" ", "", $phone);
                   $rphoneno = $rtrimmedphoneno;
                   if (substr($rtrimmedphoneno, 0,2)=="07")
                   {
                       $rphoneno = (int)"254".substr($rtrimmedphoneno, -9);
                   }
                   else if (substr($rtrimmedphoneno, 0,4)=="+254")
                   {
                       $rphoneno = (int)substr($rtrimmedphoneno, -12);
                   }
                                       else {
                                               $rphoneno = $rtrimmedphoneno;
                                       }
									   return $rphoneno;
 }				
			
		function send_sms($phoneno,$message){
	$phone = $this->clean_phone($phoneno);
	$this->main_model->send_sms($message,$phone);
		//echo 'sms successfully sen\'t to '.$phone.'<br>';
		
			}

function stripe(){
	
	
	
	
  $token  = $_POST['stripeToken'];

  $customer = Stripe_Customer::create(array(
      'email' => 'evansonbiwot@gmail.com',
      'card'  => $token
  ));

  $charge = Stripe_Charge::create(array(
      'customer' => $customer->id,
      'amount'   => 5000,
      'currency' => 'usd'
  ));


var_dump ($_POST);
  echo '<h1>Successfully charged $50.00!</h1>'; 
	}
 
 }
 ?>