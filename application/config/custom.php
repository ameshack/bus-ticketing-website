<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Custom config file
|--------------------------------------------------------------------------

|
*/

/*
|---------------------------------------------------------------------------------
| 'facebook_link'	:	A facebook link to be loaded
|
| 'twitter_link'	:	A Twitter link to be loaded
|---------------------------------------------------------------------------------
*/

$config['facebook_link']	= 'https://www.facebook.com/ModernCoastExp';

$config['twitter_link']	 = 'https://twitter.com/Modern_Coast';

$config['blog_link']	 = 'http://www.moderncoastexpress.com/blog.php';

$config['skype_link']	 = 'http://www.skype.com/@myticket';

$config['secret_key']	 = 'sk_test_XhjzDIqEluJdNho05r3qkIsJ';

$config['publishable_key']	 = 'pk_test_yPgWqMpbt0j2oniIFOaQVzcN'; 


/*
|---------------------------------------------------------------------------------
| 'page_mode' : Select whether to load mobile, desktop, and automatic page .  
|  choose between the following
|  mobile	:	To load only mobile pages
|  desktop	:	To load only desktop full size pages
|  auto		:	To automatically switch between mobile and desktop pages  (recomended)
|---------------------------------------------------------------------------------
*/
$config['page_mode']  = 'auto';

/*
|---------------------------------------------------------------------------------
| 'dev_mode' : Select whether to use dev data or live data .  
|  choose between the following
|  live	:	Pull data from moderncoasts live server ( quite slow, doesn't work offline )
|  dev	:	uses hard coded data ( much faster, works offline )
| 
|---------------------------------------------------------------------------------
*/
$config['dev_mode']  = 'live';

