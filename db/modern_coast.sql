-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 16, 2014 at 04:10 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `modern_coast`
--

-- --------------------------------------------------------

--
-- Table structure for table `town`
--

CREATE TABLE IF NOT EXISTS `town` (
  `town_id` int(11) NOT NULL AUTO_INCREMENT,
  `town_name` varchar(100) NOT NULL,
  `location` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone_line` varchar(200) DEFAULT NULL,
  `operating_time` varchar(200) DEFAULT NULL,
  `radius` int(100) NOT NULL DEFAULT '5000',
  `status` smallint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`town_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `town`
--

INSERT INTO `town` (`town_id`, `town_name`, `location`, `address`, `phone_line`, `operating_time`, `radius`, `status`) VALUES
(1, 'mombasa', '-4.052835,39.666705', NULL, '254 20 202 3775,254 722 206 423,254 733 612 265', '24 Hrs', 5000, 1),
(2, 'nairobi', '-1.2819,36.827971', NULL, '254 726 978 852 , 254 733 612 261', '24 Hrs', 5000, 1),
(3, 'kisumu', '-0.102871,34.75133', NULL, '254 716 817 400 , 254 736 077 473', '8:00 AM to 5:00PM', 5000, 1),
(4, 'malindi', '-3.218903,40.120337', NULL, NULL, '8:00 AM to 5:00PM', 500000, 1),
(5, 'kisii', '-0.673412,34.773289', NULL, '254 712 360 691,254 739 789 107', '8:00 AM to 5:00PM', 50000, 1),
(6, 'busia', '0.460648,34.111684', NULL, '254 721 274 444,254 734 126 953', '8:00 AM to 5:00PM', 50000, 1),
(7, 'migori', '-1.069233,34.469217', NULL, '254 712 361 391,254 739 789 108', '8:00 AM to 5:00PM', 50000, 1),
(8, 'nakuru', '-0.28457,36.067404', NULL, '254 720 751 437,254 736 602 971', '8:00 AM to 5:00PM', 50000, 1),
(9, 'eldoret', '0.518783,35.269079', NULL, '254 737 890 308,254 715 206 057', '8:00 AM to 5:00PM', 5000, 1),
(11, 'kampala', '0.315727,32.589885', NULL, '256 75 534 7214,256 70 169 9712,256 77 955 7089', '8:00 AM to 5:00PM', 50000, 1),
(12, 'bungoma', '0.561007,34.560938', NULL, '254 726 111 977,254 731 470 000', '8:00 AM to 5:00PM', 50000, 1),
(13, 'webuye', '0.598143,34.769962', NULL, '254 714 321 967,254 734 980 515', '8:00 AM to 5:00PM', 50000, 1),
(15, 'malaba', '0.633911,34.274059', NULL, '254 722 718 114,254 737 890 322', '24 Hrs', 50000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `to_do`
--

CREATE TABLE IF NOT EXISTS `to_do` (
  `to_do_id` int(11) NOT NULL AUTO_INCREMENT,
  `town_id` int(11) NOT NULL,
  `to_do` varchar(100) NOT NULL,
  `status` smallint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`to_do_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `to_do`
--

INSERT INTO `to_do` (`to_do_id`, `town_id`, `to_do`, `status`) VALUES
(1, 1, 'Nairobi National Museum', 1),
(2, 1, 'Nairobi National Park', 1),
(3, 1, 'Giraffe Center', 1),
(4, 1, 'Carnivore Restaurant', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
